package services

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"gitlab.com/techasit.bas/ibanking/repositories"
)

type EmployeeService interface {
	New(employee NewEmployeeRequest) (*EmployeeResponse, error)
	Get(employee uint) (*EmployeeResponse, error)
	GetAll() ([]EmployeeResponse, error)
	Leave(employee uint) error
	Update(employee NewEmployeeRequest, employeeID uint) (*EmployeeResponse, error)
}

type NewEmployeeRequest struct {
	Name        string `json:"name" validate:"required"`
	StartSalary string `json:"start_salary" validate:"required,minsalary,numeric"`
	Position    string `json:"position" validate:"required"`
	DateOfBirth string `json:"date_of_birth" validate:"required,dob"`
	BranchID    uint   `json:"branch_id" validate:"required"`
	UserID      uint   `json:"user_id"`
	User        struct {
		Username string `json:"username"`
		Password string `json:"password"`
		RoleID   uint   `json:"role_id"`
	} `json:"user"`
}

func (ne *NewEmployeeRequest) ToEmployee() repositories.Employee {
	dateOfBirth, _ := time.Parse("2006-01-02", ne.DateOfBirth)

	return repositories.Employee{
		Name:        ne.Name,
		Salary:      ne.StartSalary,
		Position:    ne.Position,
		DateOfBirth: dateOfBirth,
		BranchID:    ne.BranchID,
		UserID:      ne.UserID,
	}
}

func (ne *NewEmployeeRequest) ToEmployeeWithUser() repositories.Employee {
	dateOfBirth, _ := time.Parse("2006-01-02", ne.DateOfBirth)

	return repositories.Employee{
		Name:        ne.Name,
		Salary:      ne.StartSalary,
		Position:    ne.Position,
		DateOfBirth: dateOfBirth,
		BranchID:    ne.BranchID,
		User: repositories.User{
			Username: ne.User.Username,
			Password: ne.User.Password,
			RoleID:   RoleEmployee,
		},
	}
}

type EmployeeResponse struct {
	EmployeeID  uint         `json:"employee_id"`
	Name        string       `json:"name"`
	Salary      string       `json:"salary"`
	Position    string       `json:"position"`
	DateOfBrith string       `json:"date_of_brith"`
	Age         uint         `json:"age"`
	BranchID    uint         `json:"branch"`
	User        UserResponse `json:"user"`
}

type employeeValidator struct {
	validate   *validator.Validate
	translator ut.Translator
}

func NewEmployeeValidator(validate *validator.Validate, translator ut.Translator) *employeeValidator {
	return &employeeValidator{validate: validate, translator: translator}
}

func (ev *employeeValidator) Validate(employee NewEmployeeRequest) *ErrValidateFields {
	err := ev.validate.RegisterValidation("dob", ev.validateDateTime)
	if err != nil {
		return nil
	}

	err = ev.validate.RegisterValidation("minsalary", ev.validateMinSalary)
	if err != nil {
		return nil
	}

	ev.validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		return strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
	})

	err = ev.validate.RegisterTranslation("dob", ev.translator, func(ut ut.Translator) error {
		return ut.Add("dob", "it must be a valid date in YYYY-MM-DD format", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		return "it must be a valid date in YYYY-MM-DD format"
	})
	if err != nil {
		return nil
	}

	err = ev.validate.RegisterTranslation("minsalary", ev.translator, func(ut ut.Translator) error {
		return ut.Add("minsalary", "{0} must be greater than minimus salary", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		salary := fe.Value().(uint)
		return fmt.Sprintf("given is %d less than the minimum salary of %d", salary, MinSalary)
	})
	if err != nil {
		return nil
	}

	err = ev.validate.Struct(employee)
	if err != nil {
		validateErrs := err.(validator.ValidationErrors)

		var fields []struct {
			Field string `json:"field"`
			Error string `json:"error"`
		}

		for _, ve := range validateErrs {
			fields = append(fields, struct {
				Field string `json:"field"`
				Error string `json:"error"`
			}{
				Field: ve.Field(),
				Error: ve.Translate(ev.translator),
			})
		}

		return &ErrValidateFields{
			Err:     "validation_error",
			Message: "Invalid input fields",
			Fields:  fields,
		}
	}
	return nil
}

func (ev *employeeValidator) validateDateTime(fl validator.FieldLevel) bool {
	_, err := time.Parse("2006-01-02", fl.Field().String())
	return err == nil
}

func (ev *employeeValidator) validateMinSalary(fl validator.FieldLevel) bool {
	salary, err := strconv.Atoi(fl.Field().String())
	if err != nil {
		return false
	}
	return salary >= MinSalary
}
