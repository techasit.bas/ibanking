//go:build unit

package services_test

// func TestCreateCustomer(t *testing.T) {

// 	t.Run("sucessfully create customer", func(t *testing.T) {
// 		newCust := services.NewCustomer{
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			Phone:       "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 		}

// 		mockArgument := repositories.Customer{
// 			CitizenNO:   newCust.CitizenNO,
// 			Name:        newCust.Name,
// 			Email:       newCust.Email,
// 			Address:     newCust.Address,
// 			Gender:      newCust.Gender,
// 			PhoneNumber: newCust.Phone,
// 			DateOfBirth: newCust.DateOfBirth,
// 			UserID:      newCust.UserID,
// 			EmployeeID:  123,
// 		}

// 		mockReturn := repositories.Customer{
// 			CustomerID:  1,
// 			CitizenNO:   newCust.CitizenNO,
// 			Name:        newCust.Name,
// 			Email:       newCust.Email,
// 			Address:     newCust.Address,
// 			Gender:      newCust.Gender,
// 			PhoneNumber: newCust.Phone,
// 			DateOfBirth: newCust.DateOfBirth,
// 			CreatedAt:   time.Now(),
// 			UserID:      newCust.UserID,
// 			EmployeeID:  123,
// 		}

// 		expect := services.ResCustomer{
// 			CitizenNO:   newCust.CitizenNO,
// 			Name:        newCust.Name,
// 			Email:       newCust.Email,
// 			Address:     newCust.Address,
// 			Phone:       newCust.Phone,
// 			DateOfBirth: newCust.DateOfBirth,
// 			UserID:      newCust.UserID,
// 			CreatorID:   mockArgument.EmployeeID,
// 			CreatedAt:   mockReturn.CreatedAt, // Come from the customer that inserted into db
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custService := services.NewCustomerService(custRepo)

// 		custRepo.On("Create", mockArgument).Return(&mockReturn, nil)
// 		actual, err := custService.New(newCust, uint(123))

// 		custRepo.AssertCalled(t, "Create", mockArgument)
// 		custRepo.AssertNumberOfCalls(t, "Create", 1)
// 		assert.Nil(t, err)
// 		assert.Equal(t, expect, *actual)
// 	})

// 	t.Run("failed create customer with repo error", func(t *testing.T) {
// 		newCust := services.NewCustomer{
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			Phone:       "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 		}

// 		mockArgument := repositories.Customer{
// 			CitizenNO:   newCust.CitizenNO,
// 			Name:        newCust.Name,
// 			Email:       newCust.Email,
// 			Address:     newCust.Address,
// 			Gender:      newCust.Gender,
// 			PhoneNumber: newCust.Phone,
// 			DateOfBirth: newCust.DateOfBirth,
// 			UserID:      newCust.UserID,
// 			EmployeeID:  100,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custService := services.NewCustomerService(custRepo)

// 		custRepo.On("Create", mockArgument).Return(nil, assert.AnError)
// 		actual, err := custService.New(newCust, uint(100))

// 		custRepo.AssertCalled(t, "Create", mockArgument)
// 		custRepo.AssertNumberOfCalls(t, "Create", 1)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})

// 	t.Run("failed with unprocessable entity", func(t *testing.T) {
// 		newCust := services.NewCustomer{
// 			CitizenNO:   "121234123412a",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			Phone:       "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 		}

// 		mockArgument := repositories.Customer{
// 			CitizenNO:   newCust.CitizenNO,
// 			Name:        newCust.Name,
// 			Email:       newCust.Email,
// 			Address:     newCust.Address,
// 			Gender:      newCust.Gender,
// 			PhoneNumber: newCust.Phone,
// 			DateOfBirth: newCust.DateOfBirth,
// 			UserID:      newCust.UserID,
// 			EmployeeID:  100,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custService := services.NewCustomerService(custRepo)

// 		custRepo.On("Create", mockArgument).Return(nil, assert.AnError)
// 		actual, err := custService.New(newCust, uint(100))

// 		custRepo.AssertNotCalled(t, "Create", mockArgument)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrUnprocessableEntity)
// 	})
// }

// func TestGetCustomer(t *testing.T) {
// 	t.Run("successfully get customer", func(t *testing.T) {
// 		customerID := uint(1)
// 		mockReturn := repositories.Customer{
// 			CustomerID:  1,
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			PhoneNumber: "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 			CreatedAt:   time.Now(),
// 			EmployeeID:  100,
// 		}

// 		expect := services.ResCustomer{
// 			CitizenNO:   mockReturn.CitizenNO,
// 			Name:        mockReturn.Name,
// 			Email:       mockReturn.Email,
// 			Address:     mockReturn.Address,
// 			Phone:       mockReturn.PhoneNumber,
// 			DateOfBirth: mockReturn.DateOfBirth,
// 			UserID:      mockReturn.UserID,
// 			CreatorID:   mockReturn.EmployeeID,
// 			CreatedAt:   mockReturn.CreatedAt,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRepo)

// 		custRepo.On("Get", customerID).Return(&mockReturn, nil)
// 		actual, err := custServ.Get(customerID)

// 		assert.Nil(t, err)
// 		assert.Equal(t, expect, *actual)
// 	})

// 	t.Run("failed get customer with repo error", func(t *testing.T) {
// 		customerId := uint(1)
// 		custRepo := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRepo)

// 		custRepo.On("Get", customerId).Return(nil, assert.AnError)
// 		actual, err := custServ.Get(customerId)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})

// 	t.Run("failed get customer with not found", func(t *testing.T) {
// 		customerId := uint(1)
// 		custRepo := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRepo)

// 		custRepo.On("Get", customerId).Return(nil, sql.ErrNoRows)
// 		actual, err := custServ.Get(customerId)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 	})
// }

// func TestGetAllCustomer(t *testing.T) {
// 	t.Run("successfully get all customer", func(t *testing.T) {
// 		mockReturn := []repositories.Customer{
// 			{
// 				CustomerID:  1,
// 				CitizenNO:   "1212341234123",
// 				Name:        "John Doe",
// 				Email:       "example@email.com",
// 				Address:     "Jakarta",
// 				Gender:      "Male",
// 				PhoneNumber: "1234567890",
// 				DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 				UserID:      101,
// 				CreatedAt:   time.Now(),
// 				EmployeeID:  100,
// 			},
// 			{
// 				CustomerID:  2,
// 				CitizenNO:   "1212341234123",
// 				Name:        "Anna Mara",
// 				Email:       "example1@email.com",
// 				Address:     "Bangkok",
// 				Gender:      "Female",
// 				PhoneNumber: "1234567890",
// 				DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 				UserID:      102,
// 				CreatedAt:   time.Now(),
// 				EmployeeID:  100,
// 			},
// 		}

// 		expexts := make([]services.ResCustomer, 0)
// 		for _, c := range mockReturn {
// 			expexts = append(expexts, services.ResCustomer{
// 				CitizenNO:   c.CitizenNO,
// 				Name:        c.Name,
// 				Email:       c.Email,
// 				Address:     c.Address,
// 				Phone:       c.PhoneNumber,
// 				DateOfBirth: c.DateOfBirth,
// 				UserID:      c.UserID,
// 				CreatorID:   c.EmployeeID,
// 				CreatedAt:   c.CreatedAt,
// 			})
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custRepo.On("GetAll").Return(mockReturn, nil)

// 		custServ := services.NewCustomerService(custRepo)
// 		actuals, err := custServ.GetAll()

// 		assert.Nil(t, err)
// 		assert.Equal(t, expexts, actuals)
// 	})

// 	t.Run("failed get all customer with repo error", func(t *testing.T) {
// 		custRepo := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRepo)

// 		custRepo.On("GetAll").Return(nil, assert.AnError)
// 		actuals, err := custServ.GetAll()
// 		assert.Nil(t, actuals)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})
// }

// func TestUpdateCustomer(t *testing.T) {

// 	t.Run("succesfully update customer", func(t *testing.T) {
// 		mockCustId := uint(1)
// 		exCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			PhoneNumber: "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 			CreatedAt:   time.Now(),
// 			EmployeeID:  100,
// 		}

// 		ruc := services.ReqUpdateCustomer{ // update name only from John Doe to Techasit Chinsomrong
// 			Name:    "Techasit Chinsomrong",
// 			Email:   exCust.Email,
// 			Phone:   exCust.PhoneNumber,
// 			Address: exCust.Address,
// 		}

// 		mockCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   exCust.CitizenNO,
// 			Name:        ruc.Name,
// 			Email:       exCust.Email,
// 			Address:     exCust.Address,
// 			Gender:      exCust.Gender,
// 			PhoneNumber: exCust.PhoneNumber,
// 			DateOfBirth: exCust.DateOfBirth,
// 			UserID:      exCust.UserID,
// 			EmployeeID:  exCust.EmployeeID,
// 			CreatedAt:   exCust.CreatedAt,
// 		}

// 		expect := services.ResCustomer{
// 			CitizenNO:   exCust.CitizenNO,
// 			Name:        ruc.Name,
// 			Email:       exCust.Email,
// 			Address:     exCust.Address,
// 			Phone:       exCust.PhoneNumber,
// 			DateOfBirth: exCust.DateOfBirth,
// 			UserID:      exCust.UserID,
// 			CreatorID:   exCust.EmployeeID,
// 			CreatedAt:   exCust.CreatedAt,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custRepo.On("Get", mockCustId).Return(&exCust, nil)
// 		custRepo.On("Update", mockCust).Return(&mockCust, nil)

// 		custServ := services.NewCustomerService(custRepo)

// 		actual, err := custServ.Update(mockCustId, ruc)
// 		assert.Nil(t, err)
// 		assert.Equal(t, expect, *actual)
// 	})

// 	t.Run("failed update customer with repo error when getting a existing customer", func(t *testing.T) {
// 		mockCustId := uint(1)
// 		exCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			PhoneNumber: "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 			CreatedAt:   time.Now(),
// 			EmployeeID:  100,
// 		}

// 		ruc := services.ReqUpdateCustomer{
// 			Name:    "Techasit Chinsomrong",
// 			Email:   exCust.Email,
// 			Phone:   exCust.PhoneNumber,
// 			Address: exCust.Address,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custRepo.On("Get", mockCustId).Return(nil, assert.AnError)

// 		custServ := services.NewCustomerService(custRepo)

// 		actual, err := custServ.Update(mockCustId, ruc)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})

// 	t.Run("failed update customer with not found when getting a not existing customer", func(t *testing.T) {
// 		mockCustId := uint(1)
// 		exCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			PhoneNumber: "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      101,
// 			CreatedAt:   time.Now(),
// 			EmployeeID:  100,
// 		}

// 		ruc := services.ReqUpdateCustomer{
// 			Name:    "Techasit Chinsomrong",
// 			Email:   exCust.Email,
// 			Phone:   exCust.PhoneNumber,
// 			Address: exCust.Address,
// 		}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custRepo.On("Get", mockCustId).Return(nil, sql.ErrNoRows)

// 		custServ := services.NewCustomerService(custRepo)

// 		actual, err := custServ.Update(mockCustId, ruc)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 	})

// 	t.Run("failed update customer with repo error when update customer", func(t *testing.T) {
// 		mockCustId := uint(1)
// 		exCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   "1212341234123",
// 			Name:        "John Doe",
// 			Email:       "example@email.com",
// 			Address:     "Jakarta",
// 			Gender:      "Male",
// 			PhoneNumber: "1234567890",
// 			DateOfBirth: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			UserID:      100,
// 			CreatedAt:   time.Now(),
// 			EmployeeID:  101,
// 		}

// 		ruc := services.ReqUpdateCustomer{
// 			Name:    "Techasit Chinsomrong",
// 			Email:   exCust.Email,
// 			Phone:   exCust.PhoneNumber,
// 			Address: exCust.Address,
// 		}

// 		mockCust := repositories.Customer{
// 			CustomerID:  mockCustId,
// 			CitizenNO:   exCust.CitizenNO,
// 			Name:        ruc.Name,
// 			Email:       exCust.Email,
// 			Address:     exCust.Address,
// 			Gender:      exCust.Gender,
// 			PhoneNumber: exCust.PhoneNumber,
// 			DateOfBirth: exCust.DateOfBirth,
// 			UserID:      exCust.UserID,
// 			EmployeeID:  exCust.EmployeeID,
// 			CreatedAt:   exCust.CreatedAt,
// 		}

// 		custRep := repositories.NewMockCustomerRepository()
// 		custRep.On("Get", mockCustId).Return(&exCust, nil)
// 		custRep.On("Update", mockCust).Return(nil, assert.AnError)

// 		custServ := services.NewCustomerService(custRep)

// 		actual, err := custServ.Update(mockCustId, ruc)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})

// 	t.Run("failed update customer with unprocessable enity", func(t *testing.T) {
// 		ruc := services.ReqUpdateCustomer{Name: "Techasit Chinsomrong"}

// 		custRepo := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRepo)

// 		actual, err := custServ.Update(uint(1), ruc)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrUnprocessableUpdate)
// 	})
// }

// func TestDeleteCustomer(t *testing.T) {
// 	t.Run("successfully delete customer", func(t *testing.T) {
// 		custRep := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRep)

// 		custRep.On("Delete", mock.Anything).Return(nil)

// 		err := custServ.Delete(uint(1))
// 		assert.Nil(t, err)
// 	})

// 	t.Run("failed delete customer with repo error", func(t *testing.T) {
// 		custRep := repositories.NewMockCustomerRepository()
// 		custServ := services.NewCustomerService(custRep)

// 		custRep.On("Delete", mock.Anything).Return(assert.AnError)

// 		err := custServ.Delete(uint(1))
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})
// }
