package repositories

import "github.com/stretchr/testify/mock"

type MockEmployeeRepository struct {
	mock.Mock
}

func NewMockEmployeeRepository() *MockEmployeeRepository {
	return &MockEmployeeRepository{}
}

func (m *MockEmployeeRepository) Create(employee Employee) (*Employee, error) {
	args := m.Called(employee)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Employee), args.Error(1)
}

func (m *MockEmployeeRepository) Get(id uint) (*Employee, error) {
	args := m.Called(id)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Employee), args.Error(1)
}

func (m *MockEmployeeRepository) GetAll() ([]Employee, error) {
	args := m.Called()
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]Employee), args.Error(1)
}

func (m *MockEmployeeRepository) Delete(id uint) error {
	args := m.Called(id)
	return args.Error(0)
}

func (m *MockEmployeeRepository) Update(employee Employee) (*Employee, error) {
	args := m.Called(employee)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Employee), args.Error(1)
}
