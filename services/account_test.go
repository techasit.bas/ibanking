//go:build unit

package services_test

// import (
// 	"database/sql"
// 	"ibanking/repositories"
// 	"ibanking/services"
// 	"strconv"
// 	"testing"
// 	"time"

// 	"github.com/stretchr/testify/assert"
// 	"github.com/stretchr/testify/mock"
// )

// var accounts = []repositories.Account{
// 	{
// 		AccountNO:     123456789,
// 		AccountName:   "John Doe",
// 		Balance:       5000.50,
// 		AccountType:   "Savings",
// 		AccountStatus: true,
// 		LastedUpdate:  time.Now(),
// 		CreateAt:      time.Now(),
// 		CustomerID:    1,
// 		EmployeeID:    12345,
// 	},
// 	{
// 		AccountNO:     987654321,
// 		AccountName:   "Jane Smith",
// 		Balance:       7500.25,
// 		AccountType:   "Checking",
// 		AccountStatus: true,
// 		LastedUpdate:  time.Now(),
// 		CreateAt:      time.Now(),
// 		CustomerID:    2,
// 		EmployeeID:    54321,
// 	},
// }

// var customers = []repositories.Customer{
// 	{
// 		CustomerID:  1,
// 		CitizenNO:   "123456789",
// 		Name:        "John Doe",
// 		Address:     "123 Main St, Cityville",
// 		PhoneNumber: "555-1234",
// 		Email:       "john.doe@example.com",
// 		DateOfBirth: time.Date(1990, time.January, 15, 0, 0, 0, 0, time.UTC),
// 		Gender:      "Male",
// 		CreatedAt:   time.Now(),
// 		EmployeeID:  12345,
// 		UserID:      1001,
// 	},
// 	{
// 		CustomerID:  2,
// 		CitizenNO:   "987654321",
// 		Name:        "Jane Smith",
// 		Address:     "456 Oak St, Townsville",
// 		PhoneNumber: "555-5678",
// 		Email:       "jane.smith@example.com",
// 		DateOfBirth: time.Date(1985, time.March, 22, 0, 0, 0, 0, time.UTC),
// 		Gender:      "Female",
// 		CreatedAt:   time.Now(),
// 		EmployeeID:  54321,
// 		UserID:      1002,
// 	},
// }

// func TestOpenAccount(t *testing.T) {

// 	t.Run("sucessfully open account", func(t *testing.T) {
// 		openingAccount := services.OpenAccountRequest{
// 			AccountName: accounts[0].AccountName,
// 			AccountType: accounts[0].AccountType,
// 			Amount:      accounts[0].Balance,
// 			OwnerID:     accounts[0].CustomerID,
// 			CreatorID:   accounts[0].EmployeeID,
// 		}

// 		before := repositories.Account{
// 			AccountNO:     accounts[0].AccountNO,
// 			AccountName:   openingAccount.AccountName,
// 			Balance:       openingAccount.Amount,
// 			AccountType:   openingAccount.AccountType,
// 			AccountStatus: true,
// 			CustomerID:    openingAccount.OwnerID,
// 			EmployeeID:    openingAccount.CreatorID,
// 		}

// 		expect := services.AccountResponse{
// 			AccountNO:     accounts[0].AccountNO,
// 			AccountName:   openingAccount.AccountName,
// 			AccountType:   openingAccount.AccountType,
// 			AccountStatus: accounts[0].AccountStatus,
// 			Balance:       openingAccount.Amount,
// 			CreatedAt:     accounts[0].CreateAt,
// 			OwnerID:       openingAccount.OwnerID,
// 			CreatorID:     openingAccount.CreatorID,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CreateAccount", before).Return(&accounts[0], nil)
// 		accRepo.On("IsAccountNumberExist", accounts[0].AccountNO).Return(true, nil)

// 		accServ := services.NewAccountService(accRepo)

// 		count := 0
// 		actual, err := accServ.OpenAccount(openingAccount, func() uint {
// 			count += 1
// 			return accounts[0].AccountNO
// 		})

// 		if assert.Nil(t, err) && assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, *actual)
// 			assert.Equal(t, 1, count)
// 			accRepo.AssertCalled(t, "IsAccountNumberExist", accounts[0].AccountNO)
// 		}
// 		accRepo.AssertCalled(t, "CreateAccount", before)
// 	})

// 	t.Run("failed validation to open an account", func(t *testing.T) {
// 		openingAccount := services.OpenAccountRequest{
// 			AccountType: accounts[0].AccountType,
// 			Amount:      accounts[0].Balance,
// 			OwnerID:     accounts[0].CustomerID,
// 			CreatorID:   accounts[0].EmployeeID,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.OpenAccount(openingAccount, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrUnprocessableEntity)
// 		accRepo.AssertNotCalled(t, "CreateAccount")
// 		accRepo.AssertNotCalled(t, "IsAccountNumberExist")
// 	})

// 	t.Run("failed minimum require amount to open account", func(t *testing.T) {
// 		openingAccount := services.OpenAccountRequest{
// 			AccountName: accounts[0].AccountName,
// 			AccountType: accounts[0].AccountType,
// 			Amount:      services.MinOpenAccount - 1,
// 			OwnerID:     accounts[0].CustomerID,
// 			CreatorID:   accounts[0].EmployeeID,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.OpenAccount(openingAccount, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrMinimumRequrieAmount)
// 		accRepo.AssertNotCalled(t, "CreateAccount")
// 		accRepo.AssertNotCalled(t, "IsAccountNumberExist")
// 	})

// 	t.Run("failed to open account due to repository error while check account number", func(t *testing.T) {
// 		openingAccount := services.OpenAccountRequest{
// 			AccountName: accounts[0].AccountName,
// 			AccountType: accounts[0].AccountType,
// 			Amount:      accounts[0].Balance,
// 			OwnerID:     accounts[0].CustomerID,
// 			CreatorID:   accounts[0].EmployeeID,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("IsAccountNumberExist", accounts[0].AccountNO).Return(false, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.OpenAccount(openingAccount, func() uint { return accounts[0].AccountNO })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "IsAccountNumberExist", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "CreateAccount")
// 	})

// 	t.Run("failed to open account due to repository error while create account", func(t *testing.T) {
// 		openingAccount := services.OpenAccountRequest{
// 			AccountName: accounts[0].AccountName,
// 			AccountType: accounts[0].AccountType,
// 			Amount:      accounts[0].Balance,
// 			OwnerID:     accounts[0].CustomerID,
// 			CreatorID:   accounts[0].EmployeeID,
// 		}

// 		before := repositories.Account{
// 			AccountNO:     accounts[0].AccountNO,
// 			AccountName:   openingAccount.AccountName,
// 			Balance:       openingAccount.Amount,
// 			AccountType:   openingAccount.AccountType,
// 			AccountStatus: true,
// 			CustomerID:    openingAccount.OwnerID,
// 			EmployeeID:    openingAccount.CreatorID,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("IsAccountNumberExist", accounts[0].AccountNO).Return(true, nil)
// 		accRepo.On("CreateAccount", before).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.OpenAccount(openingAccount, func() uint { return accounts[0].AccountNO })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "IsAccountNumberExist", accounts[0].AccountNO)
// 		accRepo.AssertCalled(t, "CreateAccount", before)
// 	})
// }

// func TestGetAccount(t *testing.T) {
// 	t.Run("successfully get account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccount(accounts[0].AccountNO)

// 		if assert.Nil(t, err) && assert.NotNil(t, actual) {
// 			assert.Equal(t, services.MapToAccountResponse(accounts[0]), actual)
// 		}
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 	})

// 	t.Run("failed to get account due to account not found", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, sql.ErrNoRows)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccount(accounts[0].AccountNO)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 	})

// 	t.Run("failed to get account due to repository error", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccount(accounts[0].AccountNO)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 	})
// }

// func TestGetAccounts(t *testing.T) {
// 	t.Run("successfully get accounts", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccounts").Return(accounts, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccounts()

// 		responses := make([]services.AccountResponse, 0)
// 		for _, acc := range accounts {
// 			responses = append(responses, *services.MapToAccountResponse(acc))
// 		}

// 		if assert.Nil(t, err) && assert.NotNil(t, actual) {
// 			assert.Equal(t, responses, actual)
// 		}
// 		accRepo.AssertCalled(t, "GetAccounts")
// 	})

// 	t.Run("failed to get accounts due to repository error", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccounts").Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccounts()

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "GetAccounts")
// 	})
// }

// func TestGetAccountsByOwner(t *testing.T) {
// 	t.Run("successfully get accounts by owner", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccountsByCustomerID", accounts[0].CustomerID).Return(
// 			[]repositories.Account{accounts[0]}, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccountsByOwner(accounts[0].CustomerID)

// 		expect := []services.AccountResponse{*services.MapToAccountResponse(accounts[0])}
// 		if assert.Nil(t, err) && assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, actual)
// 		}
// 		accRepo.AssertCalled(t, "GetAccountsByCustomerID", accounts[0].CustomerID)
// 	})

// 	t.Run("failed to get accounts by owner due to repository error", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccountsByCustomerID", accounts[0].CustomerID).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.GetAccountsByOwner(accounts[0].CustomerID)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "GetAccountsByCustomerID", accounts[0].CustomerID)
// 	})
// }

// func TestDeposit(t *testing.T) {
// 	t.Run("successfully deposit", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)

// 		amount := 100
// 		updatedBalance := accounts[0]
// 		updatedBalance.Balance += float64(amount)
// 		accRepo.On("UpdateAccount", updatedBalance).Return(&updatedBalance, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.Deposit(float64(amount), accounts[0].AccountNO, 101)

// 		assert.Nil(t, err)
// 		assert.True(t, actual)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertCalled(t, "UpdateAccount", updatedBalance)
// 	})

// 	t.Run("failed deposit due to account not found", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, sql.ErrNoRows)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.Deposit(100, accounts[0].AccountNO, 101)

// 		assert.False(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "UpdateAccount")
// 	})

// 	t.Run("failed deposit due to invalid amount", func(t *testing.T) {
// 		amount := -100.0
// 		accRepo := repositories.NewMockAccountRepository()
// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.Deposit(amount, accounts[0].AccountNO, 101)

// 		assert.False(t, actual)
// 		assert.ErrorIs(t, err, services.ErrInvalidAmount)
// 		accRepo.AssertNotCalled(t, "GetAccount")
// 		accRepo.AssertNotCalled(t, "UpdateAccount")
// 	})

// 	t.Run("failed deposit due to repository error while get account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.Deposit(100, accounts[0].AccountNO, 101)

// 		assert.False(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "UpdateAccount")
// 	})

// 	t.Run("failed deposit due to repository error while update account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		amount := 100
// 		updatedBalance := accounts[0]
// 		updatedBalance.Balance += float64(amount)
// 		accRepo.On("UpdateAccount", updatedBalance).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.Deposit(100, accounts[0].AccountNO, 101)

// 		assert.False(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertCalled(t, "UpdateAccount", accounts[0])
// 	})
// }

// func TestCreateWithdrawal(t *testing.T) {
// 	t.Run("successfully cretae withdrawal", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}

// 		code := "123456"
// 		createAt := time.Now()
// 		expireAt := createAt.Add(15 * time.Minute)
// 		expect := services.WithdrawResponse{
// 			WithdrawCode: code,
// 			AccountNO:    req.AccountNO,
// 			Amount:       req.Amount,
// 			Phone:        customers[0].PhoneNumber,
// 			CreatedAt:    createAt,
// 			ExpiredAt:    expireAt,
// 		}

// 		before := repositories.Withdrawal{
// 			Code:        code,
// 			AccountNO:   req.AccountNO,
// 			Phone:       customers[0].PhoneNumber,
// 			Amount:      req.Amount,
// 			Description: req.Description,
// 			Status:      services.StatusPending,
// 		}

// 		after := repositories.Withdrawal{
// 			Code:        code,
// 			AccountNO:   req.AccountNO,
// 			Phone:       customers[0].PhoneNumber,
// 			Amount:      req.Amount,
// 			Description: req.Description,
// 			Status:      services.StatusPending,
// 			CreateAt:    createAt,
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetOwnerAccount", accounts[0].AccountNO).Return(&customers[0], nil)
// 		accRepo.On("CreateWithdrawal", before).Return(&after, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		c, _ := strconv.Atoi(code)
// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return uint(c) })

// 		assert.Nil(t, err)
// 		assert.Equal(t, expect, *actual)
// 	})

// 	t.Run("failed deposit due to invalid amount", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      -100,
// 			Description: "๋Pay for traval",
// 		}
// 		accRepo := repositories.NewMockAccountRepository()
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrInvalidAmount)

// 		accRepo.AssertNotCalled(t, "GetAccount")
// 		accRepo.AssertNotCalled(t, "GetOwnerAccount")
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed deposit due to account not found", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, sql.ErrNoRows)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)

// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "GetOwnerAccount")
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed create withdrawal due to repository error while getting account", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "GetOwnerAccount")
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed create withdrawal due to repository error while getting owner account", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetOwnerAccount", accounts[0].AccountNO).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertCalled(t, "GetOwnerAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed create withdrawal due to balance insufficient", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      accounts[0].Balance + 1,
// 			Description: "๋Pay for traval",
// 		}
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetOwnerAccount", accounts[0].AccountNO).Return(&customers[0], nil)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrInsufficientBalance)

// 		accRepo.AssertCalled(t, "GetAccount", accounts[0].AccountNO)
// 		accRepo.AssertNotCalled(t, "GetOwnerAccount")
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed create withdrawal due to repo error while getting owner customer", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetOwnerAccount", accounts[0].AccountNO).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertExpectations(t)
// 		accRepo.AssertNotCalled(t, "CreateWithdrawal")
// 	})

// 	t.Run("failed create withdrawal due to repo error while creating withdrawal", func(t *testing.T) {
// 		req := services.WithdrawRequest{
// 			AccountNO:   accounts[0].AccountNO,
// 			Amount:      100,
// 			Description: "๋Pay for traval",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetOwnerAccount", accounts[0].AccountNO).Return(&customers[0], nil)
// 		accRepo.On("CreateWithdrawal", mock.Anything).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.CreateWithdrawal(req, func() uint { return 0 })

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertExpectations(t)
// 		accRepo.AssertCalled(t, "CreateWithdrawal", mock.Anything)
// 	})

// }

// func TestConfirmWithdrawal(t *testing.T) {
// 	t.Run("successfully confirm withdrawal", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber

// 		withdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusPending,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(15 * time.Minute),
// 		}

// 		sucessWithdraw := withdraw
// 		sucessWithdraw.Status = services.StatusSuccess

// 		accountAfterWithdraw := accounts[0]
// 		accountAfterWithdraw.Balance -= 100

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&withdraw, nil)
// 		accRepo.On("CheckBalance", withdraw.AccountNO, withdraw.Amount).Return(true, nil)
// 		accRepo.On("UpdateWithdrawal", sucessWithdraw).Return(&accountAfterWithdraw, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, err)
// 		assert.Equal(t, accountAfterWithdraw.Balance, actual.Balance)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed confirm withdrawal due to invalid code", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(nil, sql.ErrNoRows)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed confirm withdrawal due to it expired", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber
// 		expiredWithdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusPending,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(-15 * time.Minute),
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&expiredWithdraw, nil)
// 		accServ := services.NewAccountService(accRepo)

// 		actual, err := accServ.ConfirmWithdraw(code, phone)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrExpiredCode)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to confirm withdrawal due to insufficient balance", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber
// 		withdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusPending,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(15 * time.Minute),
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&withdraw, nil)
// 		accRepo.On("CheckBalance", withdraw.AccountNO, withdraw.Amount).Return(false, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		actual, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrInsufficientBalance)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to confirm withdrawal due to repo error while getting withdrawal", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actaul, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actaul)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 	})

// 	t.Run("failed to confirm withdrawal due to repo error while checking balance", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber
// 		withdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusPending,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(15 * time.Minute),
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&withdraw, nil)
// 		accRepo.On("CheckBalance", withdraw.AccountNO, withdraw.Amount).Return(false, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actaul, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actaul)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to confirm withdrawal due to repo error while updating withdrawal", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber
// 		withdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusPending,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(15 * time.Minute),
// 		}

// 		beforeUpdate := withdraw
// 		beforeUpdate.Status = services.StatusSuccess

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&withdraw, nil)
// 		accRepo.On("CheckBalance", withdraw.AccountNO, withdraw.Amount).Return(true, nil)
// 		accRepo.On("UpdateWithdrawal", beforeUpdate).Return(nil, assert.AnError)
// 		accServ := services.NewAccountService(accRepo)

// 		actaul, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actaul)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to confirm withdrawal due to the code is used", func(t *testing.T) {
// 		code := "123456"
// 		phone := customers[0].PhoneNumber
// 		usedWithdraw := repositories.Withdrawal{
// 			Code:      code,
// 			AccountNO: accounts[0].AccountNO,
// 			Phone:     phone,
// 			Amount:    100,
// 			Status:    services.StatusSuccess,
// 			CreateAt:  time.Now(),
// 			ExpiredAt: time.Now().Add(15 * time.Minute),
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetWithdrawal", code, phone).Return(&usedWithdraw, nil)
// 		accRepo.On("CheckBalance", usedWithdraw.AccountNO, usedWithdraw.Amount).Return(true, nil)
// 		accServ := services.NewAccountService(accRepo)

// 		actaul, err := accServ.ConfirmWithdraw(code, phone)

// 		assert.Nil(t, actaul)
// 		assert.ErrorIs(t, err, services.ErrCodeUsed)

// 		accRepo.AssertExpectations(t)
// 	})
// }

// func TestTransfer(t *testing.T) {
// 	t.Run("successfully transfer", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "เงินสดเข้าบัญชี",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetAccount", tranReq.ToAccountNO).Return(&accounts[1], nil)
// 		fromAccount := accounts[0]
// 		fromAccount.Balance -= amount
// 		toAccount := accounts[1]
// 		toAccount.Balance += amount
// 		accRepo.On("UpdateAccount", fromAccount).Return(&fromAccount, nil)
// 		accRepo.On("UpdateAccount", toAccount).Return(&toAccount, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.Nil(t, err)
// 		assert.True(t, isTransfered)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed transfer due to insufficient balance", func(t *testing.T) {
// 		amount := accounts[0].Balance + 1.0 // transfer amount more than the current balance
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(false, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 		assert.ErrorIs(t, err, services.ErrInsufficientBalance)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed transfer due to repo error while checking balance", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(false, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 		assert.ErrorIs(t, err, services.ErrRepository)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed transfer due to repo error while updating source transer account", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetAccount", tranReq.ToAccountNO).Return(&accounts[1], nil)
// 		fromAccount := accounts[0]
// 		fromAccount.Balance -= amount
// 		accRepo.On("UpdateAccount", fromAccount).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 	})

// 	t.Run("failed transfer due to repo error while get source account", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 	})

// 	t.Run("failed transfer due to repo error while get destination account", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetAccount", tranReq.ToAccountNO).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 	})

// 	t.Run("failed transfer due to repo error while updating source account", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetAccount", tranReq.ToAccountNO).Return(&accounts[1], nil)
// 		fromAccount := accounts[0]
// 		fromAccount.Balance -= amount
// 		accRepo.On("UpdateAccount", fromAccount).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 	})

// 	t.Run("failed transfer due to repo error while updating destination account", func(t *testing.T) {
// 		amount := 100.0
// 		tranReq := services.TransferRequest{
// 			FromAccountNO: accounts[0].AccountNO,
// 			ToAccountNO:   accounts[1].AccountNO,
// 			Amount:        amount,
// 			Description:   "",
// 		}

// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("CheckBalance", tranReq.FromAccountNO, tranReq.Amount).Return(true, nil)
// 		accRepo.On("GetAccount", tranReq.FromAccountNO).Return(&accounts[0], nil)
// 		accRepo.On("GetAccount", tranReq.ToAccountNO).Return(&accounts[1], nil)
// 		fromAccount := accounts[0]
// 		fromAccount.Balance -= amount
// 		toAccount := accounts[1]
// 		toAccount.Balance += amount
// 		accRepo.On("UpdateAccount", fromAccount).Return(&fromAccount, nil)
// 		accRepo.On("UpdateAccount", toAccount).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isTransfered, err := accServ.Transfer(tranReq)

// 		assert.NotNil(t, err)
// 		assert.False(t, isTransfered)
// 	})
// }

// func TestCloseAccount(t *testing.T) {
// 	t.Run("successfully close account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		closedAccount := accounts[0]
// 		closedAccount.AccountStatus = false
// 		accRepo.On("UpdateAccount", closedAccount).Return(&closedAccount, nil)

// 		accServ := services.NewAccountService(accRepo)
// 		isClosed, err := accServ.CloseAccount(accounts[0].AccountNO)

// 		assert.Nil(t, err)
// 		assert.True(t, isClosed)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to close account due to account is not found", func(t *testing.T) {
// 		notExistAccountNo := uint(22222222)
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", notExistAccountNo).Return(nil, sql.ErrNoRows)

// 		accServ := services.NewAccountService(accRepo)
// 		_, err := accServ.CloseAccount(notExistAccountNo)

// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to close account due to repository error while getting account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isClosed, err := accServ.CloseAccount(accounts[0].AccountNO)

// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrRepository)
// 		}
// 		assert.False(t, isClosed)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to close account due to repository error while updating account", func(t *testing.T) {
// 		accounts[0].AccountStatus = true // set account status to open
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)
// 		closedAccount := accounts[0]
// 		closedAccount.AccountStatus = false
// 		accRepo.On("UpdateAccount", closedAccount).Return(nil, assert.AnError)

// 		accServ := services.NewAccountService(accRepo)
// 		isClosed, err := accServ.CloseAccount(accounts[0].AccountNO)

// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrRepository)
// 		}
// 		assert.False(t, isClosed)

// 		accRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed to close account due to repository error while updating closed account", func(t *testing.T) {
// 		accRepo := repositories.NewMockAccountRepository()
// 		accRepo.On("GetAccount", accounts[0].AccountNO).Return(&accounts[0], nil)

// 		accServ := services.NewAccountService(accRepo)
// 		isClosed, err := accServ.CloseAccount(accounts[0].AccountNO)

// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrClosedAccount)
// 		}
// 		assert.False(t, isClosed)

// 		accRepo.AssertExpectations(t)
// 	})
// }
