package repositories

import "github.com/stretchr/testify/mock"

type MockCustomerRepository struct {
	mock.Mock
}

func NewMockCustomerRepository() *MockCustomerRepository {
	return &MockCustomerRepository{}
}

func (mcr *MockCustomerRepository) Create(customer Customer) (*Customer, error) {
	args := mcr.Called(customer)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Customer), args.Error(1)
}

func (mcr *MockCustomerRepository) Get(customerId uint) (*Customer, error) {
	args := mcr.Called(customerId)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Customer), args.Error(1)
}

func (mcr *MockCustomerRepository) GetAll() ([]Customer, error) {
	args := mcr.Called()
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]Customer), args.Error(1)
}

func (mcr *MockCustomerRepository) Update(customer Customer) (*Customer, error) {
	args := mcr.Called(customer)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Customer), args.Error(1)
}

func (mcr *MockCustomerRepository) Delete(customerId uint) error {
	args := mcr.Called(customerId)
	return args.Error(0)
}
