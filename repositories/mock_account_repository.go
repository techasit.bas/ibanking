package repositories

import "github.com/stretchr/testify/mock"

type MockAccountRepository struct {
	mock.Mock
}

func NewMockAccountRepository() *MockAccountRepository {
	return &MockAccountRepository{}
}

func (mar *MockAccountRepository) CreateAccount(account Account) (*Account, error) {
	args := mar.Called(account)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Account), args.Error(1)
}

func (mar *MockAccountRepository) GetAccount(accountNO uint) (*Account, error) {
	args := mar.Called(accountNO)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Account), args.Error(1)
}

func (mar *MockAccountRepository) GetAccounts() ([]Account, error) {
	args := mar.Called()
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]Account), args.Error(1)
}

func (mar *MockAccountRepository) GetOwnerAccount(accountNO uint) (*Customer, error) {
	args := mar.Called(accountNO)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Customer), args.Error(1)
}

func (mar *MockAccountRepository) UpdateAccount(account Account) (*Account, error) {
	args := mar.Called(account)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Account), args.Error(1)
}

func (mar *MockAccountRepository) GetAccountsByCustomerID(customerID uint) ([]Account, error) {
	args := mar.Called(customerID)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]Account), args.Error(1)
}

func (mar *MockAccountRepository) CheckBalance(accountNO uint, amount float64) (bool, error) {
	args := mar.Called(accountNO, amount)
	return args.Bool(0), args.Error(1)
}

func (mar *MockAccountRepository) IsAccountNumberExist(accountNO uint) (bool, error) {
	args := mar.Called(accountNO)
	return args.Bool(0), args.Error(1)
}

func (mar *MockAccountRepository) CreateWithdrawal(wd PreWithdrawal) (*PreWithdrawal, error) {
	args := mar.Called(wd)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*PreWithdrawal), args.Error(1)
}

func (mar *MockAccountRepository) GetWithdrawal(code, phone string) (*PreWithdrawal, error) {
	args := mar.Called(code, phone)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*PreWithdrawal), args.Error(1)
}

func (mar *MockAccountRepository) UpdateWithdrawal(wd PreWithdrawal) (*Account, error) {
	args := mar.Called(wd)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Account), args.Error(1)
}
