FROM golang:1.22rc1-alpine3.19 as builder
WORKDIR /go/src
COPY . .
RUN go get && go build -o /go/bin/app

FROM alpine 
COPY --from=builder /go/bin/app /app
RUN chmod +x app
ENTRYPOINT [ "/app" ]



