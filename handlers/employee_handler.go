package handlers

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/services"
)

type EmployeeHandler struct {
	EmployeeService services.EmployeeService
}

func NewEmployeeHandler(service services.EmployeeService) *EmployeeHandler {
	return &EmployeeHandler{EmployeeService: service}
}

func (h *EmployeeHandler) GetEmployee(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil {
		return c.Status(ErrInvalidEmployeeID.Code).JSON(ErrorResponse{Error: "bad_request", Message: "invalid employee id"})
	}
	employee, err := h.EmployeeService.Get(uint(id))
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(employee)
}

func (h *EmployeeHandler) GetEmployees(c *fiber.Ctx) error {
	employees, err := h.EmployeeService.GetAll()
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(employees)
}

func (h *EmployeeHandler) CreateEmployee(c *fiber.Ctx) error {
	employee := services.NewEmployeeRequest{}
	if err := c.BodyParser(&employee); err != nil {
		return c.Status(ErrInvalidRequestBody.Code).JSON(ErrInvalidRequestBody)
	}
	createdEmployee, err := h.EmployeeService.New(employee)
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(createdEmployee)
}

func (h *EmployeeHandler) UpdateEmployee(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil || id == 0 {
		return c.Status(ErrInvalidEmployeeID.Code).JSON(ErrInvalidEmployeeID)
	}
	employee := services.NewEmployeeRequest{}
	if err := c.BodyParser(&employee); err != nil {
		return c.Status(ErrInvalidRequestBody.Code).JSON(ErrInvalidRequestBody)
	}
	updatedEmployee, err := h.EmployeeService.Update(employee, uint(id))
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(updatedEmployee)
}

func (h *EmployeeHandler) DeleteEmployee(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil || id == 0 {
		return c.Status(ErrInvalidRequestBody.Code).JSON(ErrInvalidRequestBody)
	}
	err = h.EmployeeService.Leave(uint(id))
	if err != nil {
		return h.handleError(c, err)
	}
	return c.SendStatus(fiber.StatusNoContent)
}

func (h *EmployeeHandler) handleError(c *fiber.Ctx, err error) error {
	fiberErr := err.(*fiber.Error)
	switch {
	case errors.Is(err, services.ErrEmployeeNotFound):
		return c.Status(fiberErr.Code).JSON(ErrorResponse{Error: "not_found", Message: fiberErr.Message})
	case errors.Is(err, services.ErrUserAlreadyExists):
		return c.Status(fiberErr.Code).JSON(ErrorResponse{Error: "already_exists", Message: fiberErr.Message})
	case errors.As(err, &services.ErrUnprocessableEntityStruct{}):
		errun := err.(services.ErrUnprocessableEntityStruct)
		return c.Status(errun.Code).JSON(errun.ErrValidateFields)
	}
	return c.Status(fiberErr.Code).JSON(ErrorResponse{Error: "internal_server_error", Message: fiberErr.Message})
}
