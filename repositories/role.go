package repositories

type Role struct {
	ID       uint
	RoleName string `gorm:"unique;not null"`
	RoleDesc string `gorm:"not null"`
}
