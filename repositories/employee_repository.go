package repositories

import (
	"errors"

	"github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

var (
	ErrCreateEmployee   = errors.New("error creating employee")
	ErrEmployeeNotFound = errors.New("employee not found")
	ErrFetchingEmployee = errors.New("error fetching employee")
	ErrUpdatingEmployee = errors.New("error updating employee")
	ErrDeletingEmployee = errors.New("error deleting employee")
	ErrDatabase         = errors.New("error database")
)

type employeeRepository struct {
	db *gorm.DB
}

func NewEmployeeRepository(db *gorm.DB) EmployeeRepository {
	return &employeeRepository{db}
}

func (er employeeRepository) Create(e Employee) (*Employee, error) {
	if err := er.db.Create(&e).Error; err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1062 {
			return nil, ErrUserAlreadyExists
		}
		return nil, NewDatabaseError("creating employee", err)
	}

	er.db.Preload("User.Role").Preload("User").First(&e, e.ID)
	return &e, nil
}

func (er employeeRepository) Get(id uint) (*Employee, error) {
	var e Employee
	if err := er.db.Preload("User.Role").Preload("User").First(&e, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrEmployeeNotFound
		}
		return nil, NewDatabaseError("fetching employee", err)
	}
	return &e, nil
}

func (er employeeRepository) GetAll() ([]Employee, error) {
	var employees []Employee
	if err := er.db.Preload("User.Role").Preload("User").Find(&employees).Error; err != nil {
		return nil, NewDatabaseError("fetching employees", err)
	}
	return employees, nil
}

func (er employeeRepository) Delete(id uint) error {
	if err := er.db.Delete(&Employee{}, id).Error; err != nil {
		return NewDatabaseError("deleting employee", err)
	}
	return nil
}

func (er employeeRepository) Update(e Employee) (*Employee, error) {
	if err := er.db.Model(&Employee{}).Updates(&e).Error; err != nil {
		return nil, NewDatabaseError("updating employee", err)
	}
	return &e, nil
}
