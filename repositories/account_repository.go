package repositories

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	StatusSuccess = "success"
	StatusPending = "pending"
	StatusExpired = "expired"

	TypeWithdrawal = "withdrawal"
	TypeDeposit    = "deposit"
	TypeTransfer   = "transfer"
)

var (
	// Business error
	ErrAccountNotFound     = errors.New("account not found")
	ErrCloseAccount        = errors.New("close account failed")
	ErrCheckingBalance     = errors.New("checking balance failed")
	ErrInsufficientBalance = errors.New("insufficient balance")
	ErrWithdrawCodeExpired = errors.New("withdraw code expired")
	ErrWithdrawCodeUsed    = errors.New("withdraw code used")

	// Technical error
	ErrCheckingDuplicateAccountNo = errors.New("checking duplicate account no failed")
	ErrRetrivalAccount            = errors.New("retrival account failed")
	ErrUpdateAccount              = errors.New("update account failed")
	ErrCreateAccount              = errors.New("create account failed")
	ErrCreatePreWithdrawal        = errors.New("create pre-withdrawal failed")
	ErrRetrivalPreWithdrawal      = errors.New("retrival pre-withdrawal failed")
	ErrCreateWithdrawCode         = errors.New("create withdraw code failed")
	ErrPreWithdrawalNotFound      = errors.New("pre-withdrawal not found")
	ErrWithdrawCodeNotFound       = errors.New("withdraw code not found")
	ErrUpdateWithdrawStatus       = errors.New("update withdraw status failed")
	ErrBeginWithdraw              = errors.New("begin withdraw failed")
	ErrRetrivalWithdrawCode       = errors.New("retrival withdraw code failed")
	ErrBeginDiposit               = errors.New("begin deposit failed")
	ErrBeginTransfer              = errors.New("begin transfer failed")
	ErrRetrivalCustomer           = errors.New("retrival customer failed")
)

type NotCreateTransaction struct {
	Type string `json:"type"`
}

func (t *NotCreateTransaction) Error() string {
	return fmt.Sprintf("can't create trasaction %s", t.Type)
}

type accountRepository struct {
	db *gorm.DB
}

func NewAccountRepository(db *gorm.DB) AccountRepository {
	return &accountRepository{db}
}

func (ar accountRepository) CreateAccount(acc Account) (*Account, error) {
	if err := ar.db.Create(&acc).Error; err != nil {
		return nil, NewDatabaseError("creating account", err)
	}
	return &acc, nil
}

func (ar accountRepository) GetAccount(accountNo uint) (*Account, error) {
	var account Account
	err := ar.db.First(&account, accountNo).Error
	return &account, ar.handleGetAccountError(err)
}

func (ar accountRepository) GetAccounts() ([]Account, error) {
	var accounts []Account
	if err := ar.db.Find(&accounts).Error; err != nil {
		return nil, ErrRetrivalAccount
	}
	return accounts, nil
}

func (ar accountRepository) GetAccountOwner(accountNo uint) (*Customer, error) {
	var account Account
	err := ar.db.Preload(clause.Associations).First(&account, accountNo).Error
	return &account.Customer, ar.handleGetAccountError(err)
}

func (ar accountRepository) UpdateAccount(acc Account) (*Account, error) {
	err := ar.db.Where("account_no = ?", acc.AccountNo).Updates(&acc).Error
	if err != nil {
		return nil, NewDatabaseError("updating account", err)
	}
	ar.db.First(&acc, acc.AccountNo)
	return &acc, nil
}

func (ar accountRepository) GetAccountsByCustomerID(id uint) ([]Account, error) {
	var accounts []Account
	if err := ar.db.Where("customer_id = ?", id).Find(&accounts).Error; err != nil {
		return nil, NewDatabaseError("retrival account", err)
	}
	return accounts, nil
}

func (ar accountRepository) CheckBalance(accountNo uint, amount float64) (bool, error) {
	var account Account
	err := ar.db.Select("balance").First(&account, accountNo).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, ErrAccountNotFound
		}
		return false, ErrCheckingBalance
	}
	return (account.Balance >= amount), nil
}

func (ar accountRepository) IsDuplicate(accountNo uint) (bool, error) {
	var count int64
	result := ar.db.Model(&Account{}).Where("account_no = ?", accountNo).Count(&count)
	if result.Error != nil {
		return false, ErrCheckingDuplicateAccountNo
	}
	return (count > 0), nil
}

func (ar accountRepository) Close(accountNo uint) (bool, error) {
	account := Account{AccountNo: accountNo, AccountStatus: false}
	if err := ar.db.Model(&account).Updates(account).Error; err != nil {
		return false, ErrCloseAccount
	}
	return true, nil
}

func (ar accountRepository) CreateWithdrawal(code int, prew PreWithdrawal) (*PreWithdrawal, error) {
	tx := ar.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	ok, err := ar.CheckBalance(prew.AccountNo, prew.Amount)
	if err != nil {
		return nil, NewDatabaseError("checking balance", err)
	}

	if !ok {
		return nil, ErrInsufficientBalance
	}

	if err := tx.Create(&prew).Error; err != nil {
		tx.Rollback()
		return nil, ErrCreatePreWithdrawal
	}

	var customer Customer
	err = tx.First(&customer, prew.CustomerID).Error
	if err != nil {
		tx.Rollback()
		return nil, ErrRetrivalCustomer
	}

	withdraw := WithdrawCode{
		PreWithdrawalID: prew.ID,
		Code:            strconv.Itoa(code),
		Phone:           customer.Phone,
	}
	if err := tx.Create(&withdraw).Error; err != nil {
		tx.Rollback()
		return nil, ErrCreateWithdrawCode
	}
	return &prew, tx.Commit().Error
}

func (ar accountRepository) GetPreWithdrawal(id uint) (*PreWithdrawal, error) {
	var prewithdrawal PreWithdrawal
	if err := ar.db.First(&prewithdrawal, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrPreWithdrawalNotFound
		}
		return nil, ErrRetrivalPreWithdrawal
	}
	return &prewithdrawal, nil
}

func (ar accountRepository) GetWithdrawCodeByWithdrawID(id uint) (*WithdrawCode, error) {
	var withdrawCode WithdrawCode
	if err := ar.db.First(&withdrawCode, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrWithdrawCodeNotFound
		}
		return nil, ErrRetrivalWithdrawCode
	}
	return &withdrawCode, nil
}

func (ar accountRepository) Withdraw(code, phone string) (*Account, error) {
	tx := ar.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, ErrBeginWithdraw
	}

	var wc WithdrawCode
	if err := tx.Preload(clause.Associations).First(&wc, "code = ?", code).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrWithdrawCodeNotFound
		}
		return nil, ErrRetrivalPreWithdrawal
	}

	pw := wc.PreWithdrawal
	if pw.Status == StatusExpired {
		return nil, ErrWithdrawCodeExpired
	}

	if pw.ExpiredAt.Before(time.Now()) {
		if _, err := ar.UpdateWithdrawStatus(pw.ID, StatusExpired); err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	if pw.Status == StatusPending {
		account, err := ar.debit(tx, pw.AccountNo, pw.Amount)
		if err != nil {
			tx.Rollback()
			return nil, err
		}

		if _, err := ar.UpdateWithdrawStatus(pw.ID, StatusSuccess); err != nil {
			tx.Rollback()
			return nil, err
		}

		_, err = ar.createTransaction(Transaction{
			Type:        TypeWithdrawal,
			Amount:      pw.Amount,
			Description: pw.Description,
			Withdraw: Withdraw{
				AccountNo:       pw.AccountNo,
				PreWithdrawalID: pw.ID,
			},
		})
		if err != nil {
			tx.Rollback()
			return nil, err
		}

		return account, tx.Commit().Error
	}
	return nil, ErrWithdrawCodeUsed
}

func (ar accountRepository) UpdateWithdrawStatus(withdrawID uint, status string) (bool, error) {
	if err := ar.db.Model(&PreWithdrawal{}).Where("id = ?", withdrawID).Update("status", status).Error; err != nil {
		return false, ErrUpdateWithdrawStatus
	}
	return true, nil
}

func (ar accountRepository) Deposit(amount, accountNo, operatorID uint) (*Account, error) {
	tx := ar.db.Exec("CALL deposit(@accountNo, @amount, @employee_id)",
		sql.Named("accountNo", accountNo),
		sql.Named("amount", amount),
		sql.Named("employee_id", operatorID))
	if err := tx.Error; err != nil {
		if strings.Contains(err.Error(), "not found") {
			return nil, ErrAccountNotFound
		}
		return nil, NewDatabaseError("deposit", err)
	}

	account, err := ar.GetAccount(accountNo)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (ar accountRepository) Transfer(fromAccountNo, toAccountNo uint, amount float64) (bool, error) {
	tx := ar.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return false, NewDatabaseError("begin transaction", err)
	}

	transfer := Transfer{SourceAccountNo: fromAccountNo, DestinationAccountNo: toAccountNo}

	_, err := ar.debit(tx, fromAccountNo, amount)
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("debit balance", err)
	}

	transffer, err := ar.GetAccountOwner(toAccountNo)
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("retrival account owner", err)
	}
	_, err = ar.createTransaction(Transaction{
		Type:        TypeTransfer,
		Amount:      amount,
		Description: fmt.Sprintf("Transfer %v baht to %v.", amount, transffer.Name),
		Transfer:    transfer,
	})
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("create transaction", err)
	}

	_, err = ar.topUp(tx, toAccountNo, amount)
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("top up balance", err)
	}

	payee, err := ar.GetAccountOwner(fromAccountNo)
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("retrival account owner", err)
	}

	_, err = ar.createTransaction(Transaction{
		Type:        TypeTransfer,
		Amount:      amount,
		Description: fmt.Sprintf("Received %v baht from %v.", amount, payee.Name),
		Transfer:    transfer,
	})
	if err != nil {
		tx.Rollback()
		return false, NewDatabaseError("create transaction", err)
	}
	return true, tx.Commit().Error
}

func (ar accountRepository) createTransaction(t Transaction) (*Transaction, error) {
	tx := ar.db.Begin()
	if err := tx.Create(&t).Error; err != nil {
		tx.Rollback()
		return nil, &NotCreateTransaction{Type: t.Type}
	}
	return &t, tx.Commit().Error
}

func (ar accountRepository) debit(tx *gorm.DB, accountNo uint, amount float64) (*Account, error) {
	account, err := ar.GetAccount(accountNo)
	if err != nil {
		return nil, err
	}

	if account.Balance < amount {
		return nil, ErrInsufficientBalance
	}

	account.Balance -= amount
	err = tx.Where("account_no = ?", account.AccountNo).Updates(&account).Error
	if err != nil {
		tx.Rollback()
		return nil, NewDatabaseError("updating account", err)
	}
	ar.db.First(&account, account.AccountNo)
	return account, nil
}

func (ar accountRepository) topUp(tx *gorm.DB, accountNo uint, amount float64) (*Account, error) {
	account, err := ar.GetAccount(accountNo)
	if err != nil {
		return nil, err
	}

	account.Balance += amount
	err = tx.Where("account_no = ?", account.AccountNo).Updates(&account).Error
	if err != nil {
		tx.Rollback()
		return nil, NewDatabaseError("updating account", err)
	}
	ar.db.First(&account, account.AccountNo)
	return account, nil
}

func (ar accountRepository) handleGetAccountError(err error) error {
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return ErrAccountNotFound
		}
		return NewDatabaseError("retriving account", err)
	}
	return nil
}
