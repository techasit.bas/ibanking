package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/spf13/viper"
	"gitlab.com/techasit.bas/ibanking/handlers"
	"gitlab.com/techasit.bas/ibanking/repositories"
	"gitlab.com/techasit.bas/ibanking/services"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type SqlLogger struct {
	logger.Interface
}

func (s SqlLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	sql, _ := fc()
	fmt.Printf("%v\n%s\n", sql, strings.Repeat("=", 176))
}

func main() {
	initConfig()

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		viper.GetString("mysql.username"),
		viper.GetString("mysql.password"),
		viper.GetString("mysql.host"),
		viper.GetString("mysql.port"),
		viper.GetString("mysql.database"),
	)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 SqlLogger{},
		DryRun:                 false,
	})
	if err != nil {
		panic(err)
	}

	err = db.AutoMigrate(
		&repositories.Branch{},
		&repositories.Role{},
		&repositories.User{},
		&repositories.Employee{},
		&repositories.Customer{},
		&repositories.Account{},
		&repositories.PreWithdrawal{},
		&repositories.WithdrawCode{},
		&repositories.Transaction{},
		&repositories.Transfer{},
		&repositories.Deposit{},
		&repositories.Withdraw{},
	)
	if err != nil {
		panic(err)
	}

	secret := viper.GetString("server.secret")

	app := fiber.New()

	structValidator := services.NewStructValidator()
	random := &services.Random{}
	scheduler := services.NewGoCron(time.Local)

	accRepo := repositories.NewAccountRepository(db)
	custRepo := repositories.NewCustomerRepository(db)
	emplRepo := repositories.NewEmployeeRepository(db)
	userRepo := repositories.NewUserRepository(db)
	encryptor := &services.Bcrypt{}
	auth := services.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * 24).Unix(),
	}, []byte(secret))

	accService := services.NewAccountService(services.AccountServiceDependency{
		Repository: accRepo,
		Validator:  structValidator,
		Randomer:   random,
		Scheduler:  scheduler,
	})
	custService := services.NewCustomerService(custRepo, structValidator)
	emplService := services.NewEmployeeService(emplRepo, structValidator)
	userService := services.NewUserService(services.UserServiceDependencies{
		Repository: userRepo,
		Encryptor:  encryptor,
		Auth:       auth,
		Validate:   structValidator,
	})

	accHandlers := handlers.NewAccountHandler(accService)
	custHandlers := handlers.NewCustomerHandler(custService)
	emplHanders := handlers.NewEmployeeHandler(emplService)
	userHandlers := handlers.NewUserHandler(userService)

	api := app.Group("/api")

	userV1 := api.Group("/v1/users")
	userV1.Get("/", userHandlers.GetUsers)
	userV1.Get("/:id", userHandlers.GetUser)
	userV1.Post("", userHandlers.CreateUser)
	userV1.Post("/login", userHandlers.UserLogin)

	app.Use(func(c *fiber.Ctx) error {
		token := c.Get("Authorization")
		if token == "" {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error":   "Unauthorized",
				"message": "Please login",
			})
		}

		claims, err := jwt.ParseWithClaims(token, &jwt.MapClaims{}, func(t *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
		if err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error":   "Unauthorized",
				"message": "Invalid token",
			})
		}

		expireTime, err := claims.Claims.GetExpirationTime()
		if err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error":   "Unauthorized",
				"message": "Invalid token",
			})
		}

		if expireTime.Before(time.Now()) {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error":   "Unauthorized",
				"message": "Token expired",
			})
		}
		return c.Next()
	})

	accountV1 := api.Group("/v1/accounts")
	accountV1.Post("", accHandlers.OpenAccount)
	accountV1.Get("/:accountNo", accHandlers.GetAccount)
	accountV1.Get("", accHandlers.GetAccounts)
	accountV1.Get("/owner/:ownerId", accHandlers.GetAccountsByOwner)
	accountV1.Put("/:accountNo/deposit", accHandlers.Deposit)
	accountV1.Post("/:accountNo/withdraw", accHandlers.CreateWithdrawal)
	accountV1.Put("/withdrawals/confirm", accHandlers.ConfirmWithdrawal)
	accountV1.Post("/transfer", accHandlers.Transfer)
	accountV1.Delete("/:accountNumber", accHandlers.CloseAccount)

	customerV1 := api.Group("/v1/customers")
	customerV1.Get("", custHandlers.GetCustomers)
	customerV1.Get("/:id", custHandlers.GetCustomer)
	customerV1.Post("", custHandlers.CreateCustomer)
	customerV1.Put("/:id", custHandlers.UpdateCustomer)
	customerV1.Delete("/:id", custHandlers.DeleteCustomer)

	employeeV1 := api.Group("/v1/employees")
	employeeV1.Get("", emplHanders.GetEmployees)
	employeeV1.Get("/:id", emplHanders.GetEmployee)
	employeeV1.Post("", emplHanders.CreateEmployee)
	employeeV1.Put("/:id", emplHanders.UpdateEmployee)

	if err := app.Listen(":" + viper.GetString("server.port")); err != nil {
		panic(err)
	}
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
