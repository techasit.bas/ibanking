package repositories

import "fmt"

type databaseError struct {
	cause error
	op    string
}

func NewDatabaseError(op string, err error) error {
	return &databaseError{
		cause: err,
		op:    op,
	}
}

func (e *databaseError) Error() string {
	return fmt.Sprintf("database error during %s: %v", e.op, e.cause)
}

func (e *databaseError) Unwrap() error {
	return e.cause
}
