package services

import (
	"time"

	"gitlab.com/techasit.bas/ibanking/repositories"
)

type CustomerService interface {
	New(customer NewCustomerRequest, creatorID uint) (*CustomerResponse, error)
	Get(customerID uint) (*CustomerResponse, error)
	GetAll() ([]CustomerResponse, error)
	Update(customerID uint, customer NewCustomerRequest) (*CustomerResponse, error)
	Delete(customerID uint) error
}

type NewCustomerRequest struct {
	CitizenNO   string `validate:"required,numeric,len=13" json:"citizen_no"`
	Name        string `validate:"required" json:"name"`
	Email       string `validate:"required,email" json:"email"`
	Address     string `validate:"required" json:"address"`
	Gender      string `validate:"required" json:"gender"`
	Phone       string `validate:"required,numeric,len=10" json:"phone"`
	DateOfBirth string `validate:"required" json:"date_of_birth"`
	UserID      uint   `json:"user_id"`
	User        struct {
		Username string `json:"username"`
		Password string `json:"password"`
		RoleID   uint   `json:"role_id"`
	} `json:"user"`
}

func (c *NewCustomerRequest) ToCustomer(creatorID uint, dob time.Time) *repositories.Customer {
	if c.UserID == 0 {
		return &repositories.Customer{
			CitizenNO:   c.CitizenNO,
			Name:        c.Name,
			Email:       c.Email,
			Address:     c.Address,
			Gender:      c.Gender,
			Phone:       c.Phone,
			EmployeeID:  creatorID,
			DateOfBirth: dob,
			User: repositories.User{
				Username: c.User.Username,
				Password: c.User.Password,
				RoleID:   RoleCustomer,
			},
		}
	}

	return &repositories.Customer{
		CitizenNO:   c.CitizenNO,
		Name:        c.Name,
		Email:       c.Email,
		Address:     c.Address,
		Gender:      c.Gender,
		Phone:       c.Phone,
		DateOfBirth: dob,
		UserID:      c.UserID,
	}
}

func (c *NewCustomerRequest) ToCustomerUpdate(customerID uint) *repositories.Customer {
	return &repositories.Customer{
		ID:      customerID,
		Name:    c.Name,
		Email:   c.Email,
		Address: c.Address,
		Phone:   c.Phone,
	}
}

type CustomerResponse struct {
	CitizenNO   string       `json:"citizen_no"`
	Name        string       `json:"name"`
	Email       string       `json:"email"`
	Address     string       `json:"address"`
	Gender      string       `json:"gender"`
	Phone       string       `json:"phone"`
	DateOfBirth time.Time    `json:"date_of_birth"`
	CreatedAt   time.Time    `json:"created_at"`
	User        UserResponse `json:"user"`
}
