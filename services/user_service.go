package services

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/repositories"
)

var (
	ErrUserNotFound = errors.New("user not found")
	ErrLoginFailed  = fiber.NewError(fiber.StatusUnauthorized, "username or password is incorrect")
)

type UserServiceDependencies struct {
	Repository repositories.UserRepository
	Encryptor  Encryptor
	Auth       Authenticator
	Validate   Validator
}
type userService struct {
	UserServiceDependencies
}

func NewUserService(deps UserServiceDependencies) UserService {
	return &userService{
		UserServiceDependencies: deps,
	}
}

func (us *userService) GetUser(id uint) (*UserResponse, error) {
	user, err := us.Repository.GetByID(id)
	return us.MapToResponseUser(user), us.handleError(err)
}

func (us *userService) GetUsers() ([]UserResponse, error) {
	users, err := us.Repository.GetAll()
	if err != nil {
		return nil, us.handleError(err)
	}
	userResponses := make([]UserResponse, 0, len(users))
	for _, user := range users {
		userResponse := us.MapToResponseUser(&user)
		userResponses = append(userResponses, *userResponse)
	}
	return userResponses, nil
}

func (us *userService) RegisUser(r UserRequest) (*UserResponse, error) {
	if err := us.Validate.Struct(r); err != nil {
		return nil, ErrUnprocessableEntity
	}

	registeredUser, err := us.Repository.Create(repositories.User{
		Username: r.Username,
		Password: r.Password,
		RoleID:   r.RoleID,
	})
	return us.MapToResponseUser(registeredUser), us.handleError(err)
}

func (us *userService) Login(r LoginRequest) (*LoginSuccessResponse, error) {
	if err := us.Validate.Struct(r); err != nil {
		return nil, ErrLoginFailed
	}

	userFromDB, err := us.Repository.GetByUsername(r.Username)
	if err != nil {
		if errors.Is(err, repositories.ErrUserNotFound) {
			return nil, ErrLoginFailed
		}
		return nil, us.handleError(err)
	}

	hashedPass := []byte(userFromDB.Password)
	password := []byte(r.Password)
	if err := us.Encryptor.CompareHashAndPassword(hashedPass, password); err != nil {
		return nil, ErrLoginFailed
	}

	t, err := us.Auth.GenerateToken(r.Username, userFromDB.Role.RoleName)
	if err != nil {
		return nil, ErrInternalServer
	}

	return &LoginSuccessResponse{
		User:  *us.MapToResponseUser(userFromDB),
		Token: t,
	}, nil
}

func (us userService) handleError(err error) error {
	if err == nil {
		return nil
	}

	switch {
	case errors.Is(err, repositories.ErrUserNotFound):
		return fiber.NewError(fiber.StatusNotFound, err.Error())
	case errors.Is(err, &repositories.UserAlreadyExists{}):
		return fiber.NewError(fiber.StatusConflict, err.Error())
	default:
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
}

func (us userService) MapToResponseUser(u *repositories.User) *UserResponse {
	if u == nil {
		return nil
	}

	return &UserResponse{
		ID:       u.ID,
		Username: u.Username,
		UserRole: RoleResponse{
			Name:        u.Role.RoleName,
			Description: u.Role.RoleDesc,
		},
	}
}
