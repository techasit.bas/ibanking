package services

type UserService interface {
	GetUsers() ([]UserResponse, error)
	GetUser(id uint) (*UserResponse, error)
	RegisUser(r UserRequest) (*UserResponse, error)
	Login(r LoginRequest) (*LoginSuccessResponse, error)
}

type UserResponse struct {
	ID       uint         `json:"id"`
	Username string       `json:"username"`
	UserRole RoleResponse `json:"user_role"`
}

type UserRequest struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	RoleID   uint   `json:"role_id" validate:"required"`
}

type RoleResponse struct {
	Name        string `json:"role_name"`
	Description string `json:"description"`
}

type LoginRequest struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type LoginSuccessResponse struct {
	User  UserResponse `json:"user"`
	Token string       `json:"token"`
}
