package services

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/repositories"
)

var (
	ErrCustomerNotFound = fiber.NewError(fiber.StatusNotFound, "customer not found")
	ErrUserAlreadyUsed  = fiber.NewError(fiber.StatusConflict, "user already used")
)

type customerService struct {
	repository repositories.CustomerRepository
	validator  Validator
}

func NewCustomerService(r repositories.CustomerRepository, v Validator) CustomerService {
	return &customerService{
		repository: r,
		validator:  v,
	}
}

func (cs customerService) New(r NewCustomerRequest, creatorID uint) (*CustomerResponse, error) {
	if err := cs.validator.Struct(r); err != nil {
		return nil, ErrUnprocessableEntity
	}

	dob, err := ParseDateOfBirth(r.DateOfBirth)
	if err != nil {
		return nil, ErrUnprocessableEntity
	}

	created, err := cs.repository.Create(*r.ToCustomer(creatorID, dob))
	return cs.MapToCustomerResponse(created), cs.handleError(err)
}

func (cs customerService) Get(customerID uint) (*CustomerResponse, error) {
	customer, err := cs.repository.Get(customerID)
	return cs.MapToCustomerResponse(customer), cs.handleError(err)
}

func (cs customerService) GetAll() ([]CustomerResponse, error) {
	customers, err := cs.repository.GetAll()
	if err != nil {
		return nil, cs.handleError(err)
	}
	customersRes := make([]CustomerResponse, len(customers))
	for i, customer := range customers {
		customersRes[i] = *cs.MapToCustomerResponse(&customer)
	}
	return customersRes, nil
}

func (cs customerService) Update(customerID uint, r NewCustomerRequest) (*CustomerResponse, error) {
	if err := cs.validator.Struct(r); err != nil {
		return nil, ErrUnprocessableUpdate
	}
	customer := r.ToCustomerUpdate(customerID)
	updatedCust, err := cs.repository.Update(*customer)
	return cs.MapToCustomerResponse(updatedCust), cs.handleError(err)
}

func (cs customerService) Delete(customerID uint) error {
	return cs.handleError(cs.repository.Delete(customerID))
}

func (cs customerService) handleError(err error) error {
	if err == nil {
		return nil
	}

	switch {
	case errors.Is(err, repositories.ErrUserAlreadyExists):
		return ErrUserAlreadyExists
	case errors.Is(err, repositories.ErrUserNotFound):
		return ErrUserAlreadyUsed
	case errors.Is(err, repositories.ErrCustomerNotFound):
		return ErrCustomerNotFound
	default:
		return InternalServerError
	}
}

func (cs customerService) MapToCustomerResponse(c *repositories.Customer) *CustomerResponse {
	if c == nil {
		return nil
	}

	return &CustomerResponse{
		CitizenNO:   c.CitizenNO,
		Name:        c.Name,
		Email:       c.Email,
		Phone:       c.Phone,
		Gender:      c.Gender,
		Address:     c.Address,
		DateOfBirth: c.DateOfBirth,
		CreatedAt:   c.CreatedAt,
		User: UserResponse{
			ID:       c.UserID,
			Username: c.User.Username,
			UserRole: RoleResponse{
				Name:        c.User.Role.RoleName,
				Description: c.User.Role.RoleDesc,
			},
		},
	}
}
