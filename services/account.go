package services

import "time"

type AccountService interface {
	OpenAccount(OpenAccountRequest) (*AccountResponse, error)
	GetAccount(accountNo uint) (*AccountResponse, error)
	GetAccounts() ([]AccountResponse, error)
	GetAccountsByOwner(ownerID uint) ([]AccountResponse, error)
	Deposit(amount uint, accountNo, operatorID uint) (bool, error)
	CreateWithdrawal(WithdrawRequest) (*PreWithdrawalResponse, error)
	ConfirmWithdraw(code, phone string) (*AccountResponse, error)
	Transfer(TransferRequest) (bool, error)
	CloseAccount(accountNo uint) (bool, error)
}

type OpenAccountRequest struct {
	AccountName string  `validate:"required" json:"account_name"`
	AccountType string  `validate:"required" json:"account_type"`
	Amount      float64 `validate:"required" json:"amount"`
	OwnerID     uint    `validate:"required" json:"owner_id"`
	CreatorID   uint    `validate:"required" json:"creator_id"`
}

type AccountResponse struct {
	AccountNo     uint      `json:"account_no"`
	AccountName   string    `json:"account_name"`
	AccountType   string    `json:"account_type"`
	AccountStatus bool      `json:"account_status"`
	Balance       float64   `json:"balance"`
	CreatedAt     time.Time `json:"created_at"`
	OwnerID       uint      `json:"owner_id"`
	CreatorID     uint      `json:"creator_id"`
}

type WithdrawRequest struct {
	AccountNo   uint   `validate:"required" json:"account_no"`
	Amount      uint   `validate:"required" json:"amount"`
	Description string `validate:"required" json:"description"`
	CustomerID  uint   `validate:"required" json:"customer_id"`
}

type PreWithdrawalResponse struct {
	WithdrawCode string    `json:"withdraw_code"`
	AccountNo    uint      `json:"account_no"`
	Phone        string    `json:"phone"`
	Amount       float64   `json:"amount"`
	CreatedAt    time.Time `json:"created_at"`
	ExpiredAt    time.Time `json:"expried_at"`
}

type TransferRequest struct {
	FromAccountNo uint    `validate:"required" json:"from_account"`
	ToAccountNo   uint    `validate:"required" json:"to_account"`
	Amount        float64 `validate:"required" json:"amount"`
	Description   string  `validate:"required" json:"description"`
}
