package services

import (
	"math/rand"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

const (
	MaxRetires      = 10
	MaxAccountNo    = 10
	MaxWithdrawCode = 6
	MinSalary       = 15000
	MinOpenAccount  = 500
)

const (
	RoleCustomer = iota + 1
	RoleEmployee
)

var (
	ErrSalaryBelowMinimum  = fiber.NewError(fiber.StatusUnprocessableEntity, "salary must be more than minimum salary")
	ErrNotFound            = fiber.NewError(fiber.StatusNotFound, "not found")
	ErrIncorrectPosition   = fiber.NewError(fiber.StatusUnprocessableEntity, "incorrect position")
	ErrUnprocessableUpdate = fiber.NewError(fiber.StatusUnprocessableEntity, "the update is invalid")
	ErrUnprocessableEntity = fiber.NewError(fiber.StatusUnprocessableEntity, "the entity is invalid")
	ErrInvalidAmount       = fiber.NewError(fiber.StatusBadRequest, "the amount is invalid")

	ErrExpiredCode                 = fiber.NewError(fiber.StatusUnprocessableEntity, "the code is expired")
	ErrCodeUsed                    = fiber.NewError(fiber.StatusUnprocessableEntity, "the code is used")
	ErrClosedAccount               = fiber.NewError(fiber.StatusUnprocessableEntity, "the account is closed")
	ErrInvalidUser                 = fiber.NewError(fiber.StatusUnauthorized, "the login is invalid")
	ErrAccountGenerationFailed     = fiber.NewError(fiber.StatusUnprocessableEntity, "account generation failed")
	ErrInactiveAccount             = fiber.NewError(fiber.StatusUnprocessableEntity, "the account is inactive")
	ErrInvalidCode                 = fiber.NewError(fiber.StatusUnprocessableEntity, "the code is invalid")
	ErrInvalidPhone                = fiber.NewError(fiber.StatusUnprocessableEntity, "the phone is invalid")
	ErrWithdrawalFailed            = fiber.NewError(fiber.StatusInternalServerError, "withdrawal failed")
	ErrTransferFailed              = fiber.NewError(fiber.StatusInternalServerError, "transfer failed")
	ErrClosedNotZeroBalanceAccount = fiber.NewError(fiber.StatusUnprocessableEntity, "the account is closed and not zero balance")
	ErrInternalServer              = fiber.NewError(fiber.StatusInternalServerError, "An unexpected error occurred on the server.")

	InternalServerError = fiber.NewError(fiber.StatusInternalServerError, "An unexpected error occurred on the server.")
)

type ErrUnprocessableEntityStruct struct {
	Code int
	ErrValidateFields
}

func (err ErrUnprocessableEntityStruct) Error() string {
	return err.Err
}

type ErrValidateFields struct {
	Err     string `json:"error" default:"validation_error"`
	Message string `json:"message"`
	Fields  []struct {
		Field string `json:"field"`
		Error string `json:"error"`
	}
}

func NewRepositoryError(err error) error {
	return fiber.NewError(
		fiber.StatusInternalServerError,
		err.Error(),
	)
}

func NewNotFoundError(err error) error {
	return fiber.NewError(
		fiber.StatusNotFound,
		err.Error(),
	)
}

func ParseDateOfBirth(date string) (time.Time, error) {
	return time.Parse("2006-01-02", date)
}

type Encryptor interface {
	GenerateFromPassword(password []byte, cost int) ([]byte, error)
	CompareHashAndPassword(hashedPassword, password []byte) error
}

type Bcrypt struct{}

func (c *Bcrypt) GenerateFromPassword(password []byte, cost int) ([]byte, error) {
	return bcrypt.GenerateFromPassword(password, cost)
}

func (c *Bcrypt) CompareHashAndPassword(hashedPassword, password []byte) error {
	return bcrypt.CompareHashAndPassword(hashedPassword, password)
}

type Authenticator interface {
	GenerateToken(username string, role string) (string, error)
}

type jWTAuth struct {
	Secret []byte
	Method jwt.SigningMethod
	Claims jwt.MapClaims
}

func NewWithClaims(method jwt.SigningMethod, claims jwt.MapClaims, secret []byte) Authenticator {
	return &jWTAuth{
		Method: method,
		Claims: claims,
		Secret: secret,
	}
}

func (j *jWTAuth) GenerateToken(username string, role string) (string, error) {
	j.Claims["username"] = username
	j.Claims["role"] = role
	token := jwt.NewWithClaims(j.Method, j.Claims)
	t, err := token.SignedString(j.Secret)
	if err != nil {
		return "", err
	}

	return t, nil
}

type Validator interface {
	Struct(s interface{}) error
	Var(field interface{}, tag string) error
}

type StructValidator struct {
	validate *validator.Validate
}

func NewStructValidator() *StructValidator {
	return &StructValidator{
		validate: validator.New(validator.WithRequiredStructEnabled()),
	}
}

func (sv *StructValidator) Struct(s interface{}) error {
	return sv.validate.Struct(s)
}

func (sv *StructValidator) Var(field interface{}, tag string) error {
	return sv.validate.Var(field, tag)
}

type Randomer interface {
	Uintn(numDigits int) int
}

type Random struct{}

func (*Random) Uintn(numDigits int) int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	min := 1  // Minimum value (e.g., for 3 digits, min = 100)
	max := 10 // Maximum value (e.g., for 3 digits, max = 1000)

	// Calculate the minimum and maximum based on the number of digits
	for i := 1; i < numDigits; i++ {
		min *= 10
		max *= 10
	}

	// Generate a random number in the calculated range
	return r.Intn(max-min) + min
}

type JobScheduler interface {
	Start()
	ScheduleWithLimitToRuns(jobFunc interface{}, interval interface{}, tag string, limt int) (*gocron.Job, error)
	RemoveByTag(tag string) error
}

type GoCron struct {
	scheduler *gocron.Scheduler
}

func NewGoCron(loc *time.Location) JobScheduler {
	return &GoCron{
		scheduler: gocron.NewScheduler(loc),
	}
}

func (gc *GoCron) Start() {
	if gc.scheduler.IsRunning() {
		return
	}

	gc.scheduler.StartAsync()
}

func (gc *GoCron) ScheduleWithLimitToRuns(jobFunc interface{}, interval interface{}, tag string, limit int) (*gocron.Job, error) {
	return gc.scheduler.Every(interval).Minute().LimitRunsTo(limit).WaitForSchedule().Tag(tag).Do(jobFunc)
}

func (gc *GoCron) RemoveByTag(tag string) error {
	return gc.scheduler.RemoveByTag(tag)
}
