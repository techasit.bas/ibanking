package services

import (
	"errors"
	"time"

	"github.com/go-playground/locales/en"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/repositories"

	ut "github.com/go-playground/universal-translator"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

var (
	ErrEmployeeNotFound  = fiber.NewError(fiber.StatusNotFound, "employee not found")
	ErrUserAlreadyExists = fiber.NewError(fiber.StatusConflict, "user already exists")
)

type employeeService struct {
	repository repositories.EmployeeRepository
	validator  Validator
}

func NewEmployeeService(r repositories.EmployeeRepository, v Validator) EmployeeService {
	return &employeeService{
		repository: r,
		validator:  v,
	}
}

func (es *employeeService) New(r NewEmployeeRequest) (*EmployeeResponse, error) {
	validate := validator.New()
	en := en.New()
	uni := ut.New(en, en)

	trans, _ := uni.GetTranslator("en")
	if err := en_translations.RegisterDefaultTranslations(validate, trans); err != nil {
		return nil, InternalServerError
	}

	ev := NewEmployeeValidator(validate, trans)
	errStruct := ev.Validate(r)
	if errStruct != nil {
		return nil, ErrUnprocessableEntityStruct{
			Code:              fiber.StatusUnprocessableEntity,
			ErrValidateFields: *errStruct,
		}
	}

	var employee repositories.Employee
	if r.UserID != 0 {
		employee = r.ToEmployee()
	} else {
		employee = r.ToEmployeeWithUser()
	}
	created, err := es.repository.Create(employee)
	return es.MapToResponseEmployee(created), es.handleError(err)
}

func (es employeeService) Get(employeeID uint) (*EmployeeResponse, error) {
	employee, err := es.repository.Get(employeeID)
	return es.MapToResponseEmployee(employee), es.handleError(err)
}

func (es employeeService) GetAll() ([]EmployeeResponse, error) {
	employees, err := es.repository.GetAll()
	if err := es.handleError(err); err != nil {
		return nil, err
	}

	employeeRes := make([]EmployeeResponse, len(employees))
	for i, e := range employees {
		employeeRes[i] = *es.MapToResponseEmployee(&e)
	}
	return employeeRes, nil
}

func (es employeeService) Leave(employeeID uint) error {
	return es.handleError(es.repository.Delete(employeeID))
}

func (es *employeeService) Update(employee NewEmployeeRequest, employeeID uint) (*EmployeeResponse, error) {
	updatedEmployee, err := es.repository.Update(repositories.Employee{
		Name:     employee.Name,
		Salary:   employee.StartSalary,
		Position: employee.Position,
		BranchID: employee.BranchID,
	})
	return es.MapToResponseEmployee(updatedEmployee), es.handleError(err)
}

func (es employeeService) handleError(err error) error {
	if err == nil {
		return nil
	}

	switch {
	case errors.Is(err, repositories.ErrEmployeeNotFound):
		return ErrEmployeeNotFound
	case errors.Is(err, repositories.ErrUserAlreadyExists):
		return ErrUserAlreadyExists
	default:
		return InternalServerError
	}
}

func (es employeeService) MapToResponseEmployee(e *repositories.Employee) *EmployeeResponse {
	if e == nil {
		return nil
	}

	return &EmployeeResponse{
		EmployeeID:  e.ID,
		Name:        e.Name,
		Salary:      e.Salary,
		Position:    e.Position,
		DateOfBrith: e.DateOfBirth.Format("2006-01-02"),
		BranchID:    e.BranchID,
		Age:         uint(time.Now().Year() - e.DateOfBirth.Year()),
		User: UserResponse{
			ID:       e.User.ID,
			Username: e.User.Username,
			UserRole: RoleResponse{
				Name:        e.User.Role.RoleName,
				Description: e.User.Role.RoleDesc,
			},
		},
	}
}
