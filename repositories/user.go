package repositories

import (
	"time"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	GetAll() ([]User, error)
	GetByID(id uint) (*User, error)
	GetByUsername(username string) (*User, error)
	Create(u User) (*User, error)
	Update(user User) (*User, error)
	Delete(id uint) error
}

type User struct {
	ID        uint
	Username  string `gorm:"unique; not null;size:50"`
	Password  string `gorm:"not null;column:hashed_password"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
	Role      Role
	RoleID    uint
	// Employee  Employee
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return
}
