package repositories

import (
	"time"

	"gorm.io/gorm"
)

type EmployeeRepository interface {
	Create(e Employee) (*Employee, error)
	Get(id uint) (*Employee, error)
	GetAll() ([]Employee, error)
	Delete(id uint) error
	Update(e Employee) (*Employee, error)
}

type Employee struct {
	ID          uint
	Name        string `gorm:"not null;size:50"`
	Salary      string `gorm:"size:50"`
	Position    string `gorm:"not null;size:50"`
	DateOfBirth time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
	Branch      Branch
	BranchID    uint
	User        User
	UserID      uint `gorm:"unique"`
}
