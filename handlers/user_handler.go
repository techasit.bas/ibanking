package handlers

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/services"
)

type userHandler struct {
	serv services.UserService
}

func NewUserHandler(serv services.UserService) *userHandler {
	return &userHandler{serv: serv}
}

func (h userHandler) GetUser(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil {
		return h.handleResponseError(fiber.ErrBadRequest, c)
	}

	userResponse, err := h.serv.GetUser(uint(id))
	if err != nil {
		return h.handleResponseError(err, c)
	}
	return c.JSON(userResponse)
}

func (h userHandler) GetUsers(c *fiber.Ctx) error {
	users, err := h.serv.GetUsers()
	if err != nil {
		return h.handleResponseError(err, c)
	}
	return c.JSON(users)
}

func (h userHandler) CreateUser(c *fiber.Ctx) error {
	var userRequest services.UserRequest
	if err := c.BodyParser(&userRequest); err != nil {
		return h.handleResponseError(fiber.ErrBadRequest, c)
	}

	userResponse, err := h.serv.RegisUser(userRequest)
	if err != nil {
		return h.handleResponseError(err, c)
	}
	return c.JSON(userResponse)
}

func (h userHandler) UserLogin(c *fiber.Ctx) error {
	var userRequest services.LoginRequest
	if err := c.BodyParser(&userRequest); err != nil {
		return h.handleResponseError(fiber.ErrBadRequest, c)
	}

	userResponse, err := h.serv.Login(userRequest)
	if err != nil {
		return h.handleResponseError(err, c)
	}
	return c.JSON(userResponse)
}

func (h *userHandler) handleResponseError(err error, c *fiber.Ctx) error {
	fiberError, ok := err.(*fiber.Error)
	if ok {
		return c.Status(fiberError.Code).JSON(fiber.Map{"error": strings.ToLower(fiberError.Message)})
	}
	return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
}
