package handlers

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/services"
)

type CustomerHandler struct {
	CustomerService services.CustomerService
}

func NewCustomerHandler(cs services.CustomerService) CustomerHandler {
	return CustomerHandler{CustomerService: cs}
}

func (h *CustomerHandler) GetCustomers(c *fiber.Ctx) error {
	customers, err := h.CustomerService.GetAll()
	if err != nil {
		return h.handlerError(c, err)
	}
	return c.JSON(customers)
}

func (h *CustomerHandler) GetCustomer(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bad_request",
			Message: "invalid customer id",
		})
	}
	customer, err := h.CustomerService.Get(uint(id))
	if err != nil {
		return h.handlerError(c, err)
	}
	return c.JSON(customer)
}

func (h *CustomerHandler) CreateCustomer(c *fiber.Ctx) error {
	creatorID, err := strconv.ParseUint(c.Get("X-Creator-ID"), 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "missing_creator_id",
			Message: "Creator ID is missing or invalid",
		})
	}

	customer := services.NewCustomerRequest{}
	if err := c.BodyParser(&customer); err != nil {
		fmt.Println(err)
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bad_request",
			Message: "invalid request body",
		})
	}
	createdCustomer, err := h.CustomerService.New(customer, uint(creatorID))
	if err != nil {
		return h.handlerError(c, err)
	}
	return c.JSON(createdCustomer)
}

func (h *CustomerHandler) UpdateCustomer(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bad_request",
			Message: "invalid customer id",
		})
	}
	customer := services.NewCustomerRequest{}
	if err := c.BodyParser(&customer); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bad_request",
			Message: "invalid request body",
		})
	}
	updatedCustomer, err := h.CustomerService.Update(uint(id), customer)
	if err != nil {
		return h.handlerError(c, err)
	}
	return c.JSON(updatedCustomer)
}

func (h *CustomerHandler) DeleteCustomer(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bad_request",
			Message: "invalid customer id",
		})
	}
	err = h.CustomerService.Delete(uint(id))
	if err != nil {
		return h.handlerError(c, err)
	}
	return c.JSON(SuccessResponse{Message: "successfully delete customer"})
}

func (h CustomerHandler) handlerError(c *fiber.Ctx, err error) error {
	fiberError, ok := err.(*fiber.Error)
	if ok {
		switch {
		case errors.Is(err, services.ErrCustomerNotFound):
			return c.Status(fiberError.Code).JSON(ErrorResponse{Error: "not_found", Message: fiberError.Message})
		case errors.Is(err, services.ErrUserAlreadyExists):
			return c.Status(fiberError.Code).JSON(ErrorResponse{Error: "already_exists", Message: fiberError.Message})
		case errors.Is(err, services.ErrUserAlreadyUsed):
			return c.Status(fiberError.Code).JSON(ErrorResponse{Error: "already_used", Message: fiberError.Message})
		}
	}
	return c.Status(fiber.StatusInternalServerError).JSON(ErrorResponse{Error: "internal_server_error", Message: err.Error()})
}
