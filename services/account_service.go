package services

import (
	"errors"
	"log"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/repositories"
)

var (
	ErrAccountNotFound     = fiber.NewError(fiber.StatusNotFound, "account not found. please verify the account information.")
	ErrWithdrawCodeUsed    = fiber.NewError(fiber.StatusConflict, "this withdrawal code has already been used and is no longer valid. please generate a new withdrawal code.")
	ErrInsufficientBalance = fiber.NewError(fiber.StatusUnprocessableEntity, "insufficient funds. please increase your account balance.")
)

type AccountServiceDependency struct {
	Repository repositories.AccountRepository
	Validator  Validator
	Randomer   Randomer
	Scheduler  JobScheduler
}

type accountService struct {
	AccountServiceDependency
}

func NewAccountService(deps AccountServiceDependency) AccountService {
	return &accountService{AccountServiceDependency: deps}
}

func (as *accountService) OpenAccount(request OpenAccountRequest) (*AccountResponse, error) {
	if err := as.Validator.Struct(request); err != nil {
		return nil, ErrUnprocessableEntity
	}

	if request.Amount < MinOpenAccount {
		return nil, ErrInvalidAmount
	}

	accountNo := uint(0)
	for i := 0; i < MaxRetires; i++ {
		accountNo = uint(as.Randomer.Uintn(MaxAccountNo))
		exists, err := as.Repository.IsDuplicate(accountNo)
		if err := as.handleError(err); err != nil {
			return nil, err
		}
		if !exists {
			break
		}
		if i == MaxRetires-1 {
			return nil, ErrAccountGenerationFailed
		}
	}

	openedAccount, err := as.Repository.CreateAccount(repositories.Account{
		AccountNo:     accountNo,
		AccountName:   request.AccountName,
		AccountType:   request.AccountType,
		AccountStatus: true,
		Balance:       request.Amount,
		CustomerID:    request.OwnerID,
		EmployeeID:    request.CreatorID,
	})
	return as.MapToResponseAccount(openedAccount), as.handleError(err)
}

func (as accountService) GetAccount(accountNo uint) (*AccountResponse, error) {
	account, err := as.Repository.GetAccount(accountNo)
	return as.MapToResponseAccount(account), as.handleError(err)
}

func (as *accountService) GetAccounts() ([]AccountResponse, error) {
	accounts, err := as.Repository.GetAccounts()
	return as.MapToResponseAccounts(accounts), as.handleError(err)
}

func (as *accountService) GetAccountsByOwner(ownerID uint) ([]AccountResponse, error) {
	accounts, err := as.Repository.GetAccountsByCustomerID(ownerID)
	return as.MapToResponseAccounts(accounts), as.handleError(err)
}

func (as *accountService) Deposit(amount uint, accountNo, operatorID uint) (bool, error) {
	account, err := as.Repository.Deposit(amount, accountNo, operatorID)
	return account != nil, as.handleError(err)
}

func (as *accountService) CreateWithdrawal(r WithdrawRequest) (*PreWithdrawalResponse, error) {
	prewithdrawal, err := as.Repository.CreateWithdrawal(as.Randomer.Uintn(6), repositories.PreWithdrawal{
		AccountNo:   r.AccountNo,
		Amount:      float64(r.Amount),
		Status:      repositories.StatusPending,
		CustomerID:  r.CustomerID,
		Description: r.Description,
	})
	if err := as.handleError(err); err != nil {
		return nil, err
	}

	withdraw, err := as.Repository.GetWithdrawCodeByWithdrawID(prewithdrawal.ID)
	if err := as.handleError(err); err != nil {
		return nil, err
	}

	interval := int(prewithdrawal.ExpiredAt.Sub(prewithdrawal.CreatedAt).Minutes())
	tag := strconv.Itoa(int(prewithdrawal.ID)) + "_" + withdraw.Code
	cleanUpTask := func() error {
		return as.cleanUpWithdrawal(prewithdrawal.ID, tag)
	}
	if _, err := as.Scheduler.ScheduleWithLimitToRuns(cleanUpTask, interval, tag, 1); err != nil {
		log.Println(err)
		return nil, ErrInternalServer
	}
	as.Scheduler.Start()

	return as.MapToResponseWithdraw(*withdraw, *prewithdrawal), nil
}

func (as *accountService) ConfirmWithdraw(code, phone string) (*AccountResponse, error) {
	if err := as.Validator.Var(code, "len=6,numeric"); err != nil {
		return nil, ErrInvalidCode
	}

	if err := as.Validator.Var(phone, "len=10,numeric"); err != nil {
		return nil, ErrInvalidPhone
	}

	account, err := as.Repository.Withdraw(code, phone)
	if err := as.handleError(err); err != nil {
		return nil, err
	}
	return as.MapToResponseAccount(account), nil
}

func (as *accountService) Transfer(t TransferRequest) (bool, error) {
	if t.Amount < 0 {
		return false, ErrInsufficientBalance
	}
	success, err := as.Repository.Transfer(t.FromAccountNo, t.ToAccountNo, t.Amount)
	if err != nil {
		log.Println(err)
	}
	return success, as.handleError(err)
}

func (as *accountService) CloseAccount(accountNo uint) (bool, error) {
	account, err := as.Repository.GetAccount(accountNo)
	if err := as.handleError(err); err != nil {
		return false, err
	}

	if account.AccountStatus {
		if account.Balance > 0 {
			return false, ErrClosedNotZeroBalanceAccount
		}
		success, err := as.Repository.Close(accountNo)
		return success, as.handleError(err)
	}
	return false, ErrClosedAccount
}

func (as accountService) cleanUpWithdrawal(withdrawID uint, tag string) error {
	if err := as.Scheduler.RemoveByTag(tag); err != nil {
		return err
	}

	prew, err := as.Repository.GetPreWithdrawal(withdrawID)
	if err := as.handleError(err); err != nil {
		return err
	}

	if prew.Status != repositories.StatusPending {
		return nil
	}

	_, err = as.Repository.UpdateWithdrawStatus(withdrawID, repositories.StatusExpired)
	if err := as.handleError(err); err != nil {
		return err
	}
	return nil
}

func (as accountService) MapToResponseAccount(acc *repositories.Account) *AccountResponse {
	if acc == nil {
		return nil
	}

	return &AccountResponse{
		AccountNo:     acc.AccountNo,
		AccountName:   acc.AccountName,
		AccountType:   acc.AccountType,
		AccountStatus: acc.AccountStatus,
		Balance:       acc.Balance,
		OwnerID:       acc.CustomerID,
		CreatedAt:     acc.CreatedAt,
		CreatorID:     acc.EmployeeID,
	}
}

func (as accountService) MapToResponseAccounts(accounts []repositories.Account) []AccountResponse {
	if len(accounts) == 0 {
		return []AccountResponse{}
	}

	responses := make([]AccountResponse, len(accounts))
	for i, account := range accounts {
		responses[i] = *as.MapToResponseAccount(&account)
	}
	return responses
}

func (as accountService) MapToResponseWithdraw(
	withCode repositories.WithdrawCode, prewith repositories.PreWithdrawal) *PreWithdrawalResponse {
	return &PreWithdrawalResponse{
		WithdrawCode: withCode.Code,
		AccountNo:    prewith.AccountNo,
		Phone:        withCode.Phone,
		Amount:       prewith.Amount,
		CreatedAt:    prewith.CreatedAt,
		ExpiredAt:    prewith.ExpiredAt,
	}
}

func (as accountService) handleError(err error) error {
	if err != nil {
		switch {
		case errors.Is(err, repositories.ErrAccountNotFound):
			return ErrAccountNotFound
		case errors.Is(err, repositories.ErrCloseAccount),

			errors.Is(err, repositories.ErrWithdrawCodeExpired):
			return fiber.NewError(fiber.StatusBadRequest, err.Error())
		case errors.Is(err, repositories.ErrWithdrawCodeUsed):
			return ErrWithdrawCodeUsed
		case errors.Is(err, repositories.ErrInsufficientBalance):
			return ErrInsufficientBalance
		default:
			return InternalServerError
		}
	}
	return nil
}
