package handlers

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/techasit.bas/ibanking/services"
)

type AccountHandler struct {
	AccountService services.AccountService
}

func NewAccountHandler(as services.AccountService) *AccountHandler {
	return &AccountHandler{
		AccountService: as,
	}
}

func (h *AccountHandler) OpenAccount(c *fiber.Ctx) error {
	r := new(services.OpenAccountRequest)
	if err := c.BodyParser(r); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid account body",
		})
	}

	openedAccount, err := h.AccountService.OpenAccount(*r)
	if err != nil {
		return h.handleError(c, err)
	}
	return c.Status(fiber.StatusCreated).JSON(openedAccount)
}

func (h *AccountHandler) GetAccount(c *fiber.Ctx) error {
	accountNumber, err := c.ParamsInt("accountNo")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid account number",
		})
	}

	account, err := h.AccountService.GetAccount(uint(accountNumber))
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(account)
}

func (h *AccountHandler) GetAccounts(c *fiber.Ctx) error {
	accounts, err := h.AccountService.GetAccounts()
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(accounts)
}

func (h *AccountHandler) GetAccountsByOwner(c *fiber.Ctx) error {
	owner, err := c.ParamsInt("ownerId")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid owner id",
		})
	}

	accounts, err := h.AccountService.GetAccountsByOwner(uint(owner))
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(accounts)
}

func (h *AccountHandler) Deposit(c *fiber.Ctx) error {
	accountNumber, err := c.ParamsInt("accountNo")
	if err != nil || accountNumber == 0 {
		return c.Status(ErrInvalidAccountNumber.Code).JSON(ErrInvalidAccountNumber.Message)
	}

	deposit := struct {
		Amount     uint `json:"amount"`
		OperatorId uint `json:"operator_id"`
	}{}
	if err := c.BodyParser(&deposit); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid deposit body",
		})
	}

	_, err = h.AccountService.Deposit(deposit.Amount, uint(accountNumber), deposit.OperatorId)
	if err != nil {
		return h.handleError(c, err)
	}
	return c.JSON(SuccessResponse{Message: "successfully deposit"})
}

func (h *AccountHandler) CreateWithdrawal(c *fiber.Ctx) error {
	accountNumber, err := c.ParamsInt("accountNo")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid account number",
		})
	}

	withdrawal := services.WithdrawRequest{AccountNo: uint(accountNumber)}
	if err := c.BodyParser(&withdrawal); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid withdrawal body",
		})
	}

	response, err := h.AccountService.CreateWithdrawal(withdrawal)
	if err != nil {
		return h.handleError(c, err)
	}
	return c.Status(fiber.StatusCreated).JSON(response)
}

func (h *AccountHandler) ConfirmWithdrawal(c *fiber.Ctx) error {
	confirmation := struct {
		Code  string `json:"code"`
		Phone string `json:"phone"`
	}{}
	if err := c.BodyParser(&confirmation); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid confirmation body",
		})
	}

	account, err := h.AccountService.ConfirmWithdraw(confirmation.Code, confirmation.Phone)
	if err != nil {
		return h.handleError(c, err)
	}
	return c.Status(fiber.StatusCreated).JSON(account)
}

func (h *AccountHandler) Transfer(c *fiber.Ctx) error {
	transfer := new(services.TransferRequest)
	if err := c.BodyParser(transfer); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(ErrorResponse{
			Error:   "bab_request",
			Message: "invalid transfer body",
		})
	}

	if _, err := h.AccountService.Transfer(*transfer); err != nil {
		return h.handleError(c, err)
	}
	return c.Status(fiber.StatusCreated).JSON(SuccessResponse{Message: "successfully transfer"})
}

func (h *AccountHandler) CloseAccount(c *fiber.Ctx) error {
	accountNumber, err := c.ParamsInt("accountNumber")
	if err != nil || accountNumber == 0 {
		return c.Status(ErrInvalidAccountNumber.Code).JSON(ErrInvalidAccountNumber.Message)
	}

	_, err = h.AccountService.CloseAccount(uint(accountNumber))
	if err != nil {
		return err
	}
	return c.SendStatus(fiber.StatusNoContent)
}

func (h AccountHandler) handleError(c *fiber.Ctx, err error) error {
	fiberError, ok := err.(*fiber.Error)
	if ok {
		switch {
		case errors.Is(err, services.ErrAccountNotFound):
			return c.Status(fiberError.Code).JSON(ErrorResponse{
				Error:   "account_not_found",
				Message: err.Error(),
			})
		case errors.Is(err, services.ErrWithdrawCodeUsed):
			return c.Status(fiberError.Code).JSON(ErrorResponse{
				Error:   "withdraw_code_used",
				Message: err.Error(),
			})
		case errors.Is(err, services.ErrInsufficientBalance):
			return c.Status(fiberError.Code).JSON(ErrorResponse{
				Error:   "insufficient_balance",
				Message: err.Error(),
			})
		}
	}
	return c.Status(fiber.StatusInternalServerError).JSON(ErrorResponse{
		Error:   "internal_server_error",
		Message: fiberError.Error(),
	})
}
