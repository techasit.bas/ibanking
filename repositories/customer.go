package repositories

import (
	"time"

	"gorm.io/gorm"
)

type CustomerRepository interface {
	Create(customer Customer) (*Customer, error)
	Get(id uint) (*Customer, error)
	GetAll() ([]Customer, error)
	Delete(id uint) error
	Update(custoemr Customer) (*Customer, error)
}

type Customer struct {
	ID            uint
	CitizenNO     string `gorm:"unique;not null"`
	Name          string `gorm:"not null;size:50"`
	Address       string `gorm:"not null"`
	Phone         string `gorm:"unique;not null;size:15"`
	Email         string `gorm:"unique;not null;size:50"`
	DateOfBirth   time.Time
	Gender        string `gorm:"not null;size:6"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     gorm.DeletedAt `gorm:"index"`
	EmployeeID    uint
	User          User
	UserID        uint
	Accounts      []Account `gorm:"foreignKey:CustomerID"`
	PreWithdrawal PreWithdrawal
}
