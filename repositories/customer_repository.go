package repositories

import (
	"errors"
	"fmt"

	"github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

var (
	ErrCustomerNotFound = errors.New("customer not found")
	ErrFetchingCustomer = errors.New("error fetching customer")
	ErrCreateCustomer   = errors.New("error creating customer")
	ErrUpdateCustomer   = errors.New("error updating customer")
	ErrDeleteCustomer   = errors.New("error deleting customer")
)

type customerRepository struct {
	db *gorm.DB
}

func NewCustomerRepository(db *gorm.DB) CustomerRepository {
	return &customerRepository{db}
}

func (cr customerRepository) Get(id uint) (*Customer, error) {
	var customer Customer
	if err := cr.db.Preload("User.Role").Preload("User").First(&customer, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrCustomerNotFound
		}
		return nil, NewDatabaseError("fetching customer", err)
	}
	return &customer, nil
}

func (cr customerRepository) GetAll() ([]Customer, error) {
	var customers []Customer
	if err := cr.db.Preload("User.Role").Preload("User").Find(&customers).Error; err != nil {
		return nil, NewDatabaseError("fetching customers", err)
	}
	return customers, nil
}

func (cr customerRepository) Create(customer Customer) (*Customer, error) {
	if err := cr.db.Create(&customer).Error; err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok {
			if mysqlErr.Number == 1062 {
				return nil, ErrUserAlreadyExists
			}
			if mysqlErr.Number == 1452 {
				return nil, ErrUserNotFound
			}
		}
		return nil, NewDatabaseError("creating customer", err)
	}

	cr.db.Preload("User.Role").Preload("User").First(&customer, customer.ID)
	return &customer, nil
}

func (cr customerRepository) Update(customer Customer) (*Customer, error) {
	if err := cr.db.Model(&Customer{}).Where("id = ?", customer.ID).Updates(&customer).Error; err != nil {
		fmt.Println(err)
		return nil, NewDatabaseError("updating customer", err)
	}
	cr.db.Preload("User.Role").Preload("User").First(&customer, customer.ID)
	return &customer, nil
}

func (cr customerRepository) Delete(id uint) error {
	if err := cr.db.Delete(&Customer{}, id).Error; err != nil {
		return NewDatabaseError("deleting customer", err)
	}
	return nil
}
