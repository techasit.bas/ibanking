package repositories

import (
	"errors"
	"fmt"

	"github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var (
	ErrFetchingUser      = errors.New("error fetching user")
	ErrUserNotFound      = errors.New("user not found")
	ErrCreatingUser      = errors.New("error creating user")
	ErrUpdatingUser      = errors.New("error updating user")
	ErrDeletingUser      = errors.New("error deleting user")
	ErrUserAlreadyExists = errors.New("user already exists")
)

type UserAlreadyExists struct {
	Username string
}

func (e *UserAlreadyExists) Error() string {
	return fmt.Sprintf("user %s already exits", e.Username)
}

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{db}
}

func (ur userRepository) GetAll() ([]User, error) {
	var users []User
	if err := ur.db.Preload("Role").Find(&users).Error; err != nil {
		return nil, ErrFetchingUser
	}
	return users, nil
}

func (ur userRepository) GetByID(id uint) (*User, error) {
	var user User
	if err := ur.db.Preload("Role").First(&user, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, ErrUserNotFound
		}
		return nil, ErrFetchingUser
	}
	return &user, nil
}

func (ur userRepository) GetByUsername(username string) (*User, error) {
	var user User
	if err := ur.db.Preload("Role").Where("username = ?", username).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, ErrUserNotFound
		}
		return nil, ErrFetchingUser
	}
	return &user, nil
}

func (ur userRepository) Create(user User) (*User, error) {
	var resp User
	if err := ur.db.Preload(clause.Associations).Create(&user).First(&resp).Error; err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1062 {
			return nil, &UserAlreadyExists{Username: user.Username}
		}
		return nil, ErrCreatingUser
	}
	return &resp, nil
}

func (ur userRepository) Update(user User) (*User, error) {
	if err := ur.db.Model(&User{}).Updates(&user).Error; err != nil {
		return nil, ErrUpdatingUser
	}
	return &user, nil
}

func (ur userRepository) Delete(id uint) error {
	if err := ur.db.Delete(&User{}, id).Error; err != nil {
		return ErrDeletingUser
	}
	return nil
}
