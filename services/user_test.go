package services_test

// import (
// 	"database/sql"
// 	"ibanking/repositories"
// 	"ibanking/services"
// 	"testing"

// 	"github.com/stretchr/testify/assert"
// 	"github.com/stretchr/testify/mock"
// 	"golang.org/x/crypto/bcrypt"
// 	"gorm.io/gorm"
// )

// func TestGetUsers(t *testing.T) {
// 	dependencies := services.UserServiceDependencies{
// 		Encryptor: NewMockEncryptor(),
// 		Auth:      NewMockAuthenticator(),
// 	}

// 	hashedPasswrod, _ := bcrypt.GenerateFromPassword([]byte("p@ssW0rd"), bcrypt.DefaultCost)
// 	users := []repositories.User{
// 		{Model: gorm.Model{ID: uint(1)}, Username: "user_1", HashedPassword: string(hashedPasswrod)},
// 		{Model: gorm.Model{ID: uint(2)}, Username: "user_2", HashedPassword: string(hashedPasswrod)},
// 		{Model: gorm.Model{ID: uint(3)}, Username: "user_3", HashedPassword: string(hashedPasswrod)},
// 	}
// 	t.Run("successfully geting all user", func(t *testing.T) {
// 		expect := make([]services.UserResponse, 0)
// 		for _, user := range users {
// 			expect = append(expect, services.UserResponse{
// 				ID:       user.ID,
// 				Username: user.Username,
// 			})
// 		}

// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetAll").Return(users, nil)

// 		userServ := services.NewUserService(userRepo, dependencies)
// 		actual, err := userServ.GetUsers()

// 		if assert.Nil(t, err) {
// 			assert.Equal(t, expect, actual)
// 		}

// 		userRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed geting users due to repository error", func(t *testing.T) {
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetAll").Return(nil, assert.AnError)

// 		userServ := services.NewUserService(userRepo, dependencies)
// 		actual, err := userServ.GetUsers()

// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		assert.Nil(t, actual)

// 		userRepo.AssertExpectations(t)
// 	})

// 	t.Run("successfully getting a user by userid", func(t *testing.T) {
// 		userid := uint(1)
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByID", userid).Return(&users[0], nil)

// 		expect := services.UserResponse{
// 			ID:       users[0].ID,
// 			Username: users[0].Username,
// 		}

// 		userServ := services.NewUserService(userRepo, dependencies)
// 		actual, err := userServ.GetUser(userid)
// 		if assert.Nil(t, err) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 	})

// 	t.Run("failed getting a user due to not found", func(t *testing.T) {
// 		notExistUserID := uint(10)
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByID", notExistUserID).Return(nil, sql.ErrNoRows)
// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.GetUser(notExistUserID)
// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrNotFound)
// 		}
// 	})

// 	t.Run("failed getting a user due to repository error", func(t *testing.T) {
// 		userid := uint(1)
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByID", userid).Return(nil, assert.AnError)

// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.GetUser(userid)
// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrRepository)
// 		}
// 	})
// }

// func TestRegisUser(t *testing.T) {
// 	fakePassword := "fakePassword"
// 	fakeHashedPassword := []byte("fakeHashedPassword")

// 	userRequest := services.UserRequest{
// 		Username: "user_test",
// 		Password: fakePassword,
// 	}

// 	encryptor := NewMockEncryptor()
// 	encryptor.On("GenerateFromPassword", []byte(fakePassword), bcrypt.DefaultCost).Return(fakeHashedPassword, nil)

// 	dependencies := services.UserServiceDependencies{
// 		Encryptor: encryptor,
// 		Auth:      NewMockAuthenticator(),
// 	}

// 	t.Run("successfully registering new user", func(t *testing.T) {
// 		beforeCreate := repositories.User{
// 			Username:       userRequest.Username,
// 			HashedPassword: string(fakeHashedPassword),
// 		}

// 		afterCreate := repositories.User{
// 			Model:          gorm.Model{ID: 1}, // user id it is generated for database.
// 			Username:       userRequest.Username,
// 			HashedPassword: string(fakeHashedPassword),
// 		}

// 		expect := services.UserResponse{
// 			ID:       1,
// 			Username: userRequest.Username,
// 		}
// 		repo := repositories.NewMockUserRepository()
// 		repo.On("Create", beforeCreate).Return(&afterCreate, nil)
// 		serv := services.NewUserService(repo, dependencies)

// 		actual, err := serv.RegisUser(userRequest)

// 		assert.Nil(t, err)
// 		if assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 		repo.AssertExpectations(t)
// 	})

// 	t.Run("failed registering new user due to unprocessable entity", func(t *testing.T) {
// 		repo := repositories.NewMockUserRepository()
// 		serv := services.NewUserService(repo, dependencies)

// 		actual, err := serv.RegisUser(services.UserRequest{})

// 		assert.ErrorIs(t, err, services.ErrUnprocessableEntity)
// 		assert.Nil(t, actual)
// 		repo.AssertExpectations(t)
// 	})

// 	t.Run("failed registering new user due to repository error", func(t *testing.T) {
// 		repo := repositories.NewMockUserRepository()
// 		repo.On("Create", mock.Anything).Return(nil, assert.AnError)
// 		serv := services.NewUserService(repo, dependencies)

// 		actual, err := serv.RegisUser(userRequest)

// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		assert.Nil(t, actual)
// 		repo.AssertExpectations(t)
// 	})
// }

// func TestLoginUser(t *testing.T) {
// 	fakePassword := "fakePassword"
// 	fakeHashedPassword := []byte("fakeHashedPassword")

// 	token := "token"

// 	userRequest := services.UserRequest{
// 		Username: "user_test",
// 		Password: fakePassword,
// 	}

// 	encryptor := NewMockEncryptor()
// 	encryptor.On("CompareHashAndPassword", fakeHashedPassword, []byte(fakePassword)).Return(nil)

// 	auth := NewMockAuthenticator()
// 	auth.On("GenerateToken", userRequest.Username).Return(token, nil)

// 	dependencies := services.UserServiceDependencies{
// 		Encryptor: encryptor,
// 		Auth:      auth,
// 	}

// 	t.Run("successfully login user", func(t *testing.T) {
// 		user := repositories.User{
// 			Model:          gorm.Model{ID: 1},
// 			Username:       userRequest.Username,
// 			HashedPassword: string(fakeHashedPassword),
// 		}

// 		expect := services.LoginSuccessResponse{
// 			User: services.UserResponse{
// 				ID:       user.ID,
// 				Username: user.Username,
// 			},
// 			Token: token,
// 		}

// 		repo := repositories.NewMockUserRepository()
// 		repo.On("GetByUsername", userRequest.Username).Return(&user, nil)
// 		serv := services.NewUserService(repo, dependencies)

// 		actual, err := serv.Login(userRequest)

// 		assert.Nil(t, err)
// 		if assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 	})

// 	t.Run("falied login due to uprocessable entity", func(t *testing.T) {
// 		userRepo := repositories.NewMockUserRepository()
// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.Login(services.UserRequest{})

// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrUnprocessableEntity)
// 		}
// 		userRepo.AssertExpectations(t)
// 	})

// 	t.Run("failed login due to not found", func(t *testing.T) {
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByUsername", userRequest.Username).Return(nil, sql.ErrNoRows)
// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.Login(userRequest)
// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrInvalidUser)
// 		}
// 	})

// 	t.Run("failed login due to repository error", func(t *testing.T) {
// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByUsername", userRequest.Username).Return(nil, assert.AnError)
// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.Login(userRequest)
// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrRepository)
// 		}
// 	})

// 	t.Run("failed login due to incorrect password", func(t *testing.T) {
// 		user := repositories.User{
// 			Model:          gorm.Model{ID: 1},
// 			Username:       userRequest.Username,
// 			HashedPassword: string(fakeHashedPassword),
// 		}

// 		invalidUser := services.UserRequest{
// 			Username: "user_test",
// 			Password: "invalid_password",
// 		}

// 		userRepo := repositories.NewMockUserRepository()
// 		userRepo.On("GetByUsername", invalidUser.Username).Return(&user, nil)
// 		encryptor.On("CompareHashAndPassword", fakeHashedPassword, []byte(invalidUser.Password)).Return(bcrypt.ErrMismatchedHashAndPassword)
// 		userServ := services.NewUserService(userRepo, dependencies)

// 		_, err := userServ.Login(invalidUser)
// 		if assert.NotNil(t, err) {
// 			assert.ErrorIs(t, err, services.ErrInvalidUser)
// 		}
// 	})
// }

// type MockEncryptor struct {
// 	mock.Mock
// }

// func NewMockEncryptor() *MockEncryptor {
// 	return &MockEncryptor{}
// }

// func (m *MockEncryptor) GenerateFromPassword(password []byte, cost int) ([]byte, error) {
// 	args := m.Called(password, cost)
// 	return args.Get(0).([]byte), args.Error(1)
// }

// func (m *MockEncryptor) CompareHashAndPassword(hashedPassword, password []byte) error {
// 	args := m.Called(hashedPassword, password)
// 	return args.Error(0)
// }

// type MockAuthenticator struct {
// 	mock.Mock
// }

// func NewMockAuthenticator() *MockAuthenticator {
// 	return &MockAuthenticator{}
// }

// func (m *MockAuthenticator) GenerateToken(username string) (string, error) {
// 	args := m.Called(username)
// 	return args.String(0), args.Error(1)
// }
