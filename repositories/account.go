package repositories

import (
	"time"

	"gorm.io/gorm"
)

type AccountRepository interface {
	CreateAccount(Account) (*Account, error)
	GetAccount(accountNo uint) (*Account, error)
	GetAccounts() ([]Account, error)
	GetAccountOwner(accountNo uint) (*Customer, error)
	UpdateAccount(Account) (*Account, error)
	GetAccountsByCustomerID(customerID uint) ([]Account, error)
	CheckBalance(accountNO uint, amount float64) (bool, error)
	IsDuplicate(accountNo uint) (bool, error)
	Close(accountNO uint) (bool, error)

	// Witdraw
	CreateWithdrawal(code int, prew PreWithdrawal) (*PreWithdrawal, error)
	GetPreWithdrawal(id uint) (*PreWithdrawal, error)
	GetWithdrawCodeByWithdrawID(id uint) (*WithdrawCode, error)
	Withdraw(code, phone string) (*Account, error)
	UpdateWithdrawStatus(id uint, status string) (bool, error)

	// Deposit
	Deposit(amount, accountNo uint, operatorID uint) (*Account, error)

	// Transfer
	Transfer(fromAccountNo, toAccountNo uint, amount float64) (bool, error)
}

type Account struct {
	AccountNo            uint    `gorm:"primaryKey"`
	AccountName          string  `gorm:"size:100;not null"`
	Balance              float64 `gorm:"not null"`
	AccountType          string  `gorm:"size:20;not null"`
	AccountStatus        bool
	UpdatedAt            time.Time
	CreatedAt            time.Time
	DeletedAt            gorm.DeletedAt `gorm:"index"`
	Customer             Customer       `gorm:"ForeignKey:CustomerID"`
	CustomerID           uint
	EmployeeID           uint
	SourceTransfers      []Transfer `gorm:"ForeignKey:SourceAccountNo"`
	DestinationTransfers []Transfer `gorm:"ForeignKey:DestinationAccountNo"`
	Deposits             []Deposit  `gorm:"ForeignKey:AccountNo"`
	Withdrawals          []Withdraw `gorm:"ForeignKey:AccountNo"`
}

type PreWithdrawal struct {
	ID            uint
	AccountNo     uint    `gorm:"not null"`
	Amount        float64 `gorm:"not null"`
	Status        string  `gorm:"size:20"`
	Description   string  `gorm:"size:20"`
	CreatedAt     time.Time
	ExpiredAt     time.Time
	CustomerID    uint
	WithdrawCodes []WithdrawCode
}

func (prew *PreWithdrawal) BeforeCreate(tx *gorm.DB) (err error) {
	prew.ExpiredAt = time.Now().Add(time.Minute * 15)
	return
}

type Transaction struct {
	ID          uint
	Type        string  `gorm:"not null;size:10"`
	Amount      float64 `gorm:"not null"`
	Description string  `gorm:"size:100,size:10"`
	CreatedAt   time.Time
	Transfer    Transfer
	Deposit     Deposit
	Withdraw    Withdraw
}

type Transfer struct {
	TransactionID        uint `gorm:"primaryKey"`
	SourceAccountNo      uint `gorm:"primaryKey"`
	DestinationAccountNo uint `gorm:"primaryKey"`
}

type Deposit struct {
	TransactionID uint `gorm:"primaryKey"`
	AccountNo     uint `gorm:"primaryKey"`
	EmployeeID    uint `gorm:"primaryKey"`
}

type Withdraw struct {
	TransactionID   uint `gorm:"primaryKey"`
	AccountNo       uint `gorm:"primaryKey"`
	PreWithdrawalID uint `gorm:"primaryKey"`
}

type WithdrawCode struct {
	PreWithdrawal   PreWithdrawal
	PreWithdrawalID uint   `gorm:"primaryKey"`
	Code            string `gorm:"primaryKey;size:6;not null"`
	Phone           string `gorm:"not null;size:15"`
}
