package services_test

// import (
// 	"ibanking/repositories"
// 	"ibanking/services"
// 	"testing"
// 	"time"

// 	"github.com/stretchr/testify/assert"
// 	"github.com/stretchr/testify/mock"
// 	"gorm.io/gorm"
// )

// var MockedRole = repositories.Role{
// 	Model: gorm.Model{
// 		ID:        1,
// 		CreatedAt: time.Now(),
// 		UpdatedAt: time.Now(),
// 	},
// 	RoleName: "Employee",
// 	RoleDesc: "Regular employee role",
// }

// var MockedEmployees = []repositories.Employee{
// 	{
// 		Model: gorm.Model{
// 			ID:        1,
// 			CreatedAt: time.Now(),
// 			UpdatedAt: time.Now(),
// 		},
// 		Name:        "John Doe",
// 		Salary:      50000,
// 		Position:    "Software Engineer",
// 		DateOfBirth: time.Date(1990, time.January, 1, 0, 0, 0, 0, time.UTC),
// 		Branch: repositories.Branch{
// 			Model: gorm.Model{
// 				ID:        10,
// 				CreatedAt: time.Now(),
// 				UpdatedAt: time.Now(),
// 			},
// 			Location: "New York",
// 			Phone:    "123-456-7890",
// 		},
// 		User: repositories.User{
// 			Model: gorm.Model{
// 				ID:        200,
// 				CreatedAt: time.Now(),
// 				UpdatedAt: time.Now(),
// 			},
// 			Username:       "johndoe",
// 			HashedPassword: "hashedpassword1",
// 			Role:           MockedRole,
// 		},
// 	},
// 	{
// 		Model: gorm.Model{
// 			ID:        2,
// 			CreatedAt: time.Now(),
// 			UpdatedAt: time.Now(),
// 		},
// 		Name:        "Jane Smith",
// 		Salary:      60000,
// 		Position:    "Senior Software Engineer",
// 		DateOfBirth: time.Date(1985, time.February, 15, 0, 0, 0, 0, time.UTC),
// 		Branch: repositories.Branch{
// 			Model: gorm.Model{
// 				ID:        10,
// 				CreatedAt: time.Now(),
// 				UpdatedAt: time.Now(),
// 			},
// 			Location: "San Francisco",
// 			Phone:    "987-654-3210",
// 		},
// 		User: repositories.User{
// 			Model: gorm.Model{
// 				ID:        100,
// 				CreatedAt: time.Now(),
// 				UpdatedAt: time.Now(),
// 			},
// 			Username:       "janesmith",
// 			HashedPassword: "hashedpassword2",
// 			Role:           MockedRole,
// 		},
// 	},
// }

// type MockValidator struct {
// 	mock.Mock
// }

// func (m *MockValidator) Struct(s interface{}) error {
// 	args := m.Called(s)
// 	return args.Error(0)
// }

// func TestCreateEmployee(t *testing.T) {
// 	t.Run("successful created", func(t *testing.T) {
// 		employee := MockedEmployees[0]

// 		request := services.NewEmployeeRequest{
// 			Name:        employee.Name,
// 			StartSalary: employee.Salary,
// 			Position:    employee.Position,
// 			DateOfBirth: employee.DateOfBirth,
// 			BranchID:    employee.Branch.ID,
// 			UserID:      employee.User.ID,
// 		}

// 		mockedArg := repositories.Employee{
// 			Name:        request.Name,
// 			Salary:      request.StartSalary,
// 			Position:    request.Position,
// 			DateOfBirth: request.DateOfBirth,
// 			Branch:      repositories.Branch{Model: gorm.Model{ID: request.BranchID}},
// 			User:        repositories.User{Model: gorm.Model{ID: request.UserID}},
// 		}

// 		expect := services.MapToResponseEmployee(employee)

// 		repository := repositories.NewMockEmployeeRepository()
// 		validator := &MockValidator{}

// 		repository.On("Create", mockedArg).Return(&employee, nil)
// 		validator.On("Struct", request).Return(nil)

// 		service := services.NewEmployeeService(repository, validator)
// 		actual, _ := service.New(request)

// 		if assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, *actual)
// 		}

// 		repository.AssertExpectations(t)
// 	})
// }

// func TestCreateEmployee(t *testing.T) {
// 	empRepo := repositories.NewMockEmployeeRepository()
// 	empServ := services.NewEmployeeService(empRepo)

// 	t.Run("successful created", func(t *testing.T) {
// 		expect := services.NewEmployee{
// 			Name:        "John Doe",
// 			StartSalary: 50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 		}

// 		argument := repositories.Employee{
// 			Name:        expect.Name,
// 			Salary:      expect.StartSalary,
// 			Position:    expect.Position,
// 			DateOfBrith: expect.DateOfBrith,
// 			BranchID:    expect.BranchID,
// 			UserID:      expect.UserID,
// 		}
// 		empRepo.On("Create", argument).Return(&argument, nil)

// 		actual, err := empServ.New(expect)

// 		assert.Nil(t, err)
// 		empRepo.AssertCalled(t, "Create", argument)

// 		if assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 	})

// 	t.Run("failed created with empty name", func(t *testing.T) {
// 		newEmployee := services.NewEmployee{
// 			Name:        "",
// 			StartSalary: 50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 		}

// 		actual, err := empServ.New(newEmployee)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrEmptyName)
// 		empRepo.AssertNotCalled(t, "Create")
// 	})

// 	t.Run("failed created with too low salary", func(t *testing.T) {
// 		newEmployee := services.NewEmployee{
// 			Name:        "John Doe",
// 			StartSalary: services.MinSalary - 1,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 		}

// 		actual, err := empServ.New(newEmployee)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrMinSalary)
// 		empRepo.AssertNotCalled(t, "Create")
// 	})

// 	t.Run("failed created with repository error", func(t *testing.T) {
// 		newEmployee := services.NewEmployee{
// 			Name:        "John Doe",
// 			StartSalary: 50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    2,
// 			UserID:      100,
// 		}

// 		argument := repositories.Employee{
// 			Name:        newEmployee.Name,
// 			Salary:      newEmployee.StartSalary,
// 			Position:    newEmployee.Position,
// 			DateOfBrith: newEmployee.DateOfBrith,
// 			BranchID:    newEmployee.BranchID,
// 			UserID:      newEmployee.UserID,
// 		}
// 		empRepo.On("Create", argument).Return(nil, errors.New("some error"))

// 		actual, err := empServ.New(newEmployee)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		empRepo.AssertCalled(t, "Create", argument)
// 	})
// }

// func TestGetEmployee(t *testing.T) {
// 	empRepo := repositories.NewMockEmployeeRepository()
// 	empServ := services.NewEmployeeService(empRepo)

// 	t.Run("successful get", func(t *testing.T) {
// 		expect := services.ResEmployee{
// 			EmployeeID:  1,
// 			Name:        "John Doe",
// 			Salary:      50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 			Age:         time.Now().Year() - time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC).Year(),
// 		}

// 		mockReturn := repositories.Employee{
// 			EmployeeID:  expect.EmployeeID,
// 			Name:        expect.Name,
// 			Salary:      expect.Salary,
// 			Position:    expect.Position,
// 			DateOfBrith: expect.DateOfBrith,
// 			BranchID:    expect.BranchID,
// 			UserID:      expect.UserID,
// 		}

// 		id := uint(1)
// 		empRepo.On("Get", id).Return(&mockReturn, nil)

// 		actual, err := empServ.Get(id)

// 		assert.Nil(t, err)
// 		empRepo.AssertCalled(t, "Get", id)

// 		if assert.NotNil(t, *actual) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 	})

// 	t.Run("failed get with repository error", func(t *testing.T) {
// 		id := uint(2)
// 		empRepo.On("Get", id).Return(nil, errors.New("some error"))

// 		actual, err := empServ.Get(id)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		empRepo.AssertCalled(t, "Get", id)
// 	})

// 	t.Run("failed get with not found", func(t *testing.T) {
// 		id := uint(3)
// 		empRepo.On("Get", id).Return(nil, sql.ErrNoRows)

// 		actual, err := empServ.Get(id)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		empRepo.AssertCalled(t, "Get", id)
// 	})
// }

// func TestGetAllEmployees(t *testing.T) {
// 	t.Run("successful get all", func(t *testing.T) {
// 		empRepo := repositories.NewMockEmployeeRepository()
// 		empServ := services.NewEmployeeService(empRepo)
// 		expect := []services.ResEmployee{
// 			{
// 				EmployeeID:  1,
// 				Name:        "John Doe",
// 				Salary:      50000,
// 				Position:    "Developer",
// 				DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 				BranchID:    1,
// 				UserID:      101,
// 				Age:         time.Now().Year() - time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC).Year(),
// 			},
// 			{
// 				EmployeeID:  2,
// 				Name:        "Jane Doe",
// 				Salary:      60000,
// 				Position:    "Manager",
// 				DateOfBrith: time.Date(1990, 1, 1, 0, 0, 0, 0, time.UTC),
// 				BranchID:    1,
// 				UserID:      102,
// 				Age:         time.Now().Year() - time.Date(1990, 1, 1, 0, 0, 0, 0, time.UTC).Year(),
// 			},
// 		}

// 		mockReturn := []repositories.Employee{}
// 		for _, emp := range expect {
// 			mockReturn = append(mockReturn, repositories.Employee{
// 				EmployeeID:  emp.EmployeeID,
// 				Name:        emp.Name,
// 				Salary:      emp.Salary,
// 				Position:    emp.Position,
// 				DateOfBrith: emp.DateOfBrith,
// 				BranchID:    emp.BranchID,
// 				UserID:      emp.UserID,
// 			})
// 		}

// 		empRepo.On("GetAll").Return(mockReturn, nil)
// 		actual, err := empServ.GetAll()
// 		assert.Nil(t, err)
// 		empRepo.AssertCalled(t, "GetAll")

// 		if assert.NotNil(t, actual) {
// 			assert.Equal(t, expect, actual)
// 		}
// 	})

// 	t.Run("failed get all with repository error", func(t *testing.T) {
// 		empRepo := repositories.NewMockEmployeeRepository()
// 		empServ := services.NewEmployeeService(empRepo)
// 		empRepo.On("GetAll").Return(nil, errors.New("some error"))

// 		actual, err := empServ.GetAll()

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrRepository)
// 		empRepo.AssertCalled(t, "GetAll")
// 	})
// }

// func TestLeaveEmployee(t *testing.T) {
// 	empRepo := repositories.NewMockEmployeeRepository()
// 	empServ := services.NewEmployeeService(empRepo)

// 	t.Run("successful leave", func(t *testing.T) {
// 		id := uint(1)
// 		empRepo.On("Delete", id).Return(nil)

// 		err := empServ.Leave(id)

// 		assert.Nil(t, err)
// 		empRepo.AssertCalled(t, "Delete", id)
// 	})

// 	t.Run("failed leave with repository error", func(t *testing.T) {
// 		id := uint(2)
// 		empRepo.On("Delete", id).Return(errors.New("some error"))

// 		err := empServ.Leave(id)

// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		empRepo.AssertCalled(t, "Delete", id)
// 	})
// }

// func TestUpdateEmployee(t *testing.T) {
// 	empRepo := repositories.NewMockEmployeeRepository()
// 	empServ := services.NewEmployeeService(empRepo)
// 	t.Run("successfuly update employee", func(t *testing.T) {
// 		existEmp := repositories.Employee{
// 			EmployeeID:  1,
// 			Name:        "John Doe",
// 			Salary:      50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 		}
// 		latest := services.LatestEmployee{ // Update all fields
// 			Name:     "Jane Doe",
// 			Salary:   60000,
// 			Position: "Manager",
// 		}
// 		mockEmp := repositories.Employee{
// 			EmployeeID:  1,
// 			Name:        latest.Name,
// 			Salary:      latest.Salary,
// 			Position:    latest.Position,
// 			DateOfBrith: existEmp.DateOfBrith,
// 			BranchID:    existEmp.BranchID,
// 			UserID:      existEmp.UserID,
// 		}
// 		expect := services.ResEmployee{
// 			EmployeeID:  1,
// 			Name:        latest.Name,
// 			Salary:      latest.Salary,
// 			Position:    latest.Position,
// 			DateOfBrith: existEmp.DateOfBrith,
// 			BranchID:    existEmp.BranchID,
// 			UserID:      existEmp.UserID,
// 			Age:         time.Now().Year() - time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC).Year(),
// 		}

// 		id := uint(1)
// 		empRepo.On("Get", id).Return(&existEmp, nil)
// 		empRepo.On("Update", mockEmp).Return(&mockEmp, nil)

// 		actual, err := empServ.Update(latest, id)
// 		assert.Nil(t, err)
// 		if assert.NotNil(t, actual) && empRepo.AssertCalled(t, "Update", mockEmp) {
// 			assert.Equal(t, expect, *actual)
// 		}
// 	})

// 	t.Run("failed update with not existing id to be update", func(t *testing.T) {
// 		latest := services.LatestEmployee{
// 			Name:     "Jane Doe",
// 			Salary:   60000,
// 			Position: "Manager",
// 		}
// 		id := uint(2)
// 		empRepo.On("Get", id).Return(nil, sql.ErrNoRows)

// 		actual, err := empServ.Update(latest, id)

// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrNotfound)
// 		empRepo.AssertNotCalled(t, "Update")
// 	})

// 	t.Run("failed update with invalidate", func(t *testing.T) {
// 		latest := services.LatestEmployee{
// 			Name:     "Jane Doe",
// 			Salary:   services.MinSalary - 1,
// 			Position: "Manager",
// 		}
// 		id := uint(1)
// 		empRepo.AssertNotCalled(t, "Get")
// 		empRepo.AssertNotCalled(t, "Update")

// 		actual, err := empServ.Update(latest, id)
// 		assert.Nil(t, actual)
// 		assert.ErrorIs(t, err, services.ErrUnprocessableUpdate)
// 	})

// 	t.Run("failed update with repository error", func(t *testing.T) {
// 		latest := services.LatestEmployee{
// 			Name:     "Jane Doe",
// 			Salary:   60000,
// 			Position: "Manager",
// 		}
// 		id := uint(3)
// 		existEmp := repositories.Employee{
// 			EmployeeID:  id,
// 			Name:        "John Doe",
// 			Salary:      50000,
// 			Position:    "Developer",
// 			DateOfBrith: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
// 			BranchID:    1,
// 			UserID:      101,
// 		}
// 		mockEmp := repositories.Employee{
// 			EmployeeID:  3,
// 			Name:        latest.Name,
// 			Salary:      latest.Salary,
// 			Position:    latest.Position,
// 			DateOfBrith: existEmp.DateOfBrith,
// 			BranchID:    existEmp.BranchID,
// 			UserID:      existEmp.UserID,
// 		}
// 		empRepo.On("Get", id).Return(&existEmp, nil)
// 		empRepo.On("Update", mockEmp).Return(nil, errors.New("some error"))

// 		_, err := empServ.Update(latest, id)
// 		if assert.NotNil(t, err) && empRepo.AssertCalled(t, "Update", mockEmp) {
// 			assert.ErrorIs(t, err, services.ErrRepository)
// 		}
// 	})
// }
