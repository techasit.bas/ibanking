package handlers

import "github.com/gofiber/fiber/v2"

var (
	ErrInvalidEmployeeID    = fiber.NewError(fiber.StatusBadRequest, "Invalid employee ID")
	ErrInvalidAccountNumber = fiber.NewError(fiber.StatusBadRequest, "Invalid account number")
	ErrInvalidOwner         = fiber.NewError(fiber.StatusBadGateway, "Invalid owner id")
	ErrInvalidRequestBody   = fiber.NewError(fiber.StatusBadRequest, "Invalid request body")
	ErrInvalidParams        = fiber.NewError(fiber.StatusBadRequest, "Invalid params")
)

type SuccessResponse struct {
	Message string `json:"message"`
}

type ErrorResponse struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}
