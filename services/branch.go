package services

type Branch struct {
	ID       uint
	Location string
	Phone    string
}

type BranchResponse struct {
	ID       uint   `json:"id"`
	Location string `json:"location"`
	Phone    string `json:"phone"`
}
