package repositories

import (
	"time"

	"gorm.io/gorm"
)

type Branch struct {
	ID        uint
	Location  string `gorm:"not null"`
	Phone     string `gorm:"size:15"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
